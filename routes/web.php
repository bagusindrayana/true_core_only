<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Auth::user() != null){
		return redirect('home');
	}
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware' => ['auth']] , function(){
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/print/{page}/{id}', 'HomeController@printData');
	Route::Resource('/akun', 'DaftarAkunController');
	Route::Resource('/costomer', 'CostomerController');
	Route::Resource('/supplier', 'SupplierController');

	Route::post('/barang/store', 'BarangController@store');
	Route::Resource('/barang', 'BarangController');
	
	Route::Resource('/unit', 'UnitController');

	Route::Resource('/tagihan', 'TagihanController');
	Route::Resource('/pemasukan', 'PemasukanController');
	Route::Resource('/pengeluaran', 'PengeluaranController');
	Route::Resource('/jurnalumum', 'JurnalUmumController');
	Route::Resource('/detail_pemasukan', 'DetailPemasukanController');
	Route::Resource('/detail_pengeluaran', 'DetailPengeluaranController');
	Route::Resource('/detail_tagihan', 'DetailTagihanController');
	Route::Resource('/transfer', 'TransferController');

	//Route Approval Permintaan
	Route::get('/permintaan/approval/{id}','PermintaanController@approval');
	Route::Resource('/permintaan', 'PermintaanController');

	Route::get('/detail_permintaan/delete/{id}','DetailPermintaanController@destroy');
	Route::Resource('/detail_permintaan', 'DetailTagihanControllerPermintaanController');

	Route::Resource('/pembelian', 'PembelianController');
	Route::Resource('/pemakaian', 'PemakaianController');
	Route::Resource('/detail_pemakaian', 'PemakaianController');

	Route::Resource('/transaksi_kas', 'TransaksiKasController');
	Route::Resource('/buku_besar', 'BukuBesarController');
	Route::Resource('/neraca_saldo', 'NeracaController');
	Route::Resource('/laba_rugi', 'LabaRugiController');
	Route::Resource('/gudang', 'GudangController');
	
	Route::Resource('/profil', 'ProfilController');
	Route::Resource('/level', 'LevelUserController');
	Route::Resource('/user', 'UserController');
	Route::Resource('/pajak', 'PajakController');
});
