<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPermintaan extends Model
{
    protected $guarded = [];

    public function barang(){
        return $this->belongsTo('App\Barang','kode_barang','kode_barang');
    }

    public function unit(){
        return $this->belongsTo('App\Unit','kode_unit','kode_unit');
    }
}
