<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPengeluaran extends Model
{
    protected $guarded = [];

    public function unit(){
        return $this->belongsTo('App\Unit','kode_unit','kode_unit');
    }
}
