<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Barang extends Model
{
    protected $table = 'barangs';
    protected $guarded = [];

    public function kode()
    {
    	$query = DB::table('barangs')->select(DB::raw('RIGHT(barangs.kode_barang,4) as kode', FALSE))
                                       ->orderBy('kode_barang','DESC')
                                       ->limit(1)
                                       ->get();
      $count = count($query);
      if($count <> 0)
      {
          // $data = $query->toArray();
          $kode = intval($query[0]->kode) + 1;
      }
      else
      {
          $kode = 1;
      }

      $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
      $kodejadi = "BR-".$kodemax;
      return $kodejadi;
    }
}
