<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailJurnalUmum extends Model
{
    protected $guarded = [];

    public function jurnal_umum (){
    	return $this->belongsTo('App\JurnalUmum','id_jurnal_umum','id');
    }

    public function akun (){
    	return $this->belongsTo('App\Akun','id_akun','id');
    }
}
