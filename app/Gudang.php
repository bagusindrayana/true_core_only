<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gudang extends Model
{
    protected $guarded = [];

    public function barang(){
    	return $this->belongsTo(Barang::class,'kode_barang','kode_barang');
    }
}
