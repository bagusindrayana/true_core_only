<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JurnalUmum;
use App\DetailJurnalUmum;
use App\Akun;
class JurnalUmumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'jurnalumum';
        $title = 'Jurnal Umum';
     
        $jurnalumums = JurnalUmum::all();
        //dd($jurnalumums);
        return view('jurnalumum.index',compact('jurnalumums','page','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $page = 'jurnalumum';
        $title = 'Jurnal Umum';
        $akuns = Akun::all();
        return view('jurnalumum.create',compact('page','title','akuns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $id = JurnalUmum::create([
                'no_jurnal'=>$request->no_jurnal,
                'uraian'=>$request->uraian,
                'tanggal'=>$request->tanggal
        ])->id;
        for ($i=0; $i < count($request->id_akun) ; $i++) { 
            DetailJurnalUmum::create([
                'id_jurnal_umum'=>$id,
                'id_akun'=>$request->id_akun[$i],
                'debit'=>$request->debit[$i],
                'kredit'=>$request->kredit[$i],
                'uraian'=>$request->uraian_detail[$i]
            ]);
            $a1 = Akun::findOrFail($request->id_akun[$i]);
            if($a1 != null){
                $a1->update(['saldo'=>$a1->saldo-$request->kredit[$i]]);
                $a1->update(['saldo'=>$a1->saldo+$request->debit[$i]]);
            }
                
        }

        return redirect('jurnalumum');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = 'jurnalumum';
        $title = 'Jurnal Umum';
        $jurnalumum = JurnalUmum::with('detail','detail.akun')->find($id);
        
        return view('jurnalumum.view',compact('page','title','jurnalumum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'jurnalumum';
        $title = 'Jurnal Umum';
        $jurnalumum = JurnalUmum::find($id);
        $akuns = Akun::all();



        return view('jurnalumum.edit',compact('page','title','jurnalumum','akuns'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   


        JurnalUmum::find($id)->update([
                'no_jurnal'=>$request->no_jurnal,
                'uraian'=>$request->uraian,
                'tanggal'=>$request->tanggal
        ]);
        
        foreach (JurnalUmum::findOrFail($id) as $dt) {
            foreach ($dt as $data) {
                $a1 = Akun::findOrFail($data->id_akun);
                $a1->update(['saldo'=>$a1->saldo+$data->kredit]);
             
                $a2->update(['saldo'=>$a1->saldo-$data->debit]);
            }
        }
        DetailJurnalUmum::where('id_jurnal_umum',$id)->delete();
        
        
        for ($i=0; $i < count($request->id_akun) ; $i++) { 
            DetailJurnalUmum::create([
                'id_jurnal_umum'=>$id,
                'id_akun'=>$request->id_akun[$i],
                'debit'=>$request->debit[$i],
                'kredit'=>$request->kredit[$i],
                'uraian'=>$request->uraian_detail[$i]
            ]);
            $a1 = Akun::findOrFail($request->id_akun[$i]);
            $a1->update(['saldo'=>$a1->saldo-$kredit[$i]]);
         
            $a1->update(['saldo'=>$a1->saldo+$debit[$i]]);
        }
        return redirect('jurnalumum');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        JurnalUmum::find($id)->delete();
        return redirect('jurnalumum');
    }
}
