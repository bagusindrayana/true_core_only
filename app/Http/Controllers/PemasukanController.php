<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Pemasukan;
use App\DetailPemasukan;
use App\Akun;
use App\Kas;
use App\Costomer;
use App\Unit;
class PemasukanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'pemasukan';
        $title = 'Pemasukan';
     
        $pemasukans = Pemasukan::with('akun1','akun2','user')->get();
        //dd($pemasukans);

        return view('pemasukan.index',compact('pemasukans','page','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $page = 'pemasukan';
        $title = 'Pemasukan';
        $akuns = Akun::all();
        $costomers = Costomer::all();
        $units = Unit::all();
        return view('pemasukan.create',compact('page','akuns','title','costomers','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
      
        $id = Pemasukan::create([     
            'tanggal'=>$request->tanggal,
            'id_akun_1'=>$request->id_akun1,
            'id_akun_2'=>$request->id_akun2,
            'id_costomer'=>$request->id_costomer,
            
            'user_id'=>Auth::user()->id,
            'uraian'=>$request->uraian_pemasukan,
            'nomor'=>$request->nomor
        ])->id;
        $jumlah = $request->qty * $request->harga_satuan;
        DetailPemasukan::create([
            'id_pemasukan'=>$id,
            'uraian'=>$request->uraian,
            'qty'=>$request->qty,
            'kode_unit'=>$request->kode_unit,
            'harga_satuan'=>$request->harga_satuan,
            'jumlah'=>$jumlah,
        ]);
        $a1 = Akun::findOrFail($request->id_akun1);
        $a1->update(['saldo'=>$a1->saldo+$jumlah]);
        $a2 = Akun::findOrFail($request->id_akun2);
        $a2->update(['saldo'=>$a2->saldo-$jumlah]);

        return redirect('pemasukan/'.$id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = 'pemasukan';
        $title = 'Pemasukan';
        $pemasukan = Pemasukan::with('detail')->find($id);
        
       
        return view('pemasukan.view',compact('page','title','pemasukan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'pemasukan';
        $title = 'Pemasukan';
        $pemasukan = Pemasukan::with('detail')->find($id);
        $costomers = Costomer::all();
        $akuns = Akun::all();
        $units = Unit::all();
        return view('pemasukan.edit',compact('page','title','pemasukan','akuns','costomers','units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
        if ($request->tombol == 'ubah') {
            Pemasukan::where('id',$id)->update([
                    'tanggal'=>$request->tanggal,
                    'id_akun_1'=>$request->id_akun1,
                    'id_akun_2'=>$request->id_akun2,
                    'id_costomer'=>$request->id_costomer,
                    
                    'user_id'=>Auth::user()->id,
                    'uraian'=>$request->uraian_pemasukan,
                    'nomor'=>$request->nomor
                   
            ]);
            return redirect('pemasukan');
        }
        else if($request->tombol == 'tambah_detail'){
            $jumlah = $request->qty * $request->harga_satuan;
            DetailPemasukan::create([
                'id_pemasukan'=>$id,
                'uraian'=>$request->uraian,
                'qty'=>$request->qty,
                'kode_unit'=>$request->kode_unit,
                'harga_satuan'=>$request->harga_satuan,
                'jumlah'=>$jumlah,
            ]);
            $pemasukan = Pemasukan::findOrFail($id);
            $a1 = Akun::findOrFail($pemasukan->id_akun_1);
            $a1->update(['saldo'=>$a1->saldo+$jumlah]);
            $a2 = Akun::findOrFail($pemasukan->id_akun_2);
            $a2->update(['saldo'=>$a2->saldo-$jumlah]);
            return redirect('pemasukan/'.$id.'/edit');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pemasukan::find($id)->delete();
        return redirect('pemasukan');
    }
}
