<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tagihan;
use App\Costomer;
use App\DetailTagihan;
use App\Unit;
use App\Akun;
use Auth;
class TagihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'tagihan';
        $title = 'Tagihan';
        $tagihans = Tagihan::all();
        return view('tagihan.index',compact('page','title','tagihans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $page = 'tagihan';
        $title = 'Tagihan';
        $costomers = Costomer::all();
        $units = Unit::all();
        return view('tagihan.create',compact('page','title','costomers','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $invoice_old = Tagihan::orderBy('created_at','DESC')->first();
        if($invoice_old){
            $invoice_new = (int)$invoice_old->invoice + 1;
        }
        else {
            $invoice_new = 1;
        }
        
        if (strlen($invoice_new) == 1) {
            $invoice_new = '00'.$invoice_new;
        }
        elseif (strlen($invoice_new) == 2) {
           $invoice_new = '0'.$invoice_new;
        }

        $dt = Tagihan::create([
            'invoice'=>$invoice_new,
            'tanggal'=>$request->tanggal,
            'no'=>$request->no,
            'site'=>$request->site,
            'id_costomer'=>$request->id_costomer,
            
            'ppn'=>($request->ppn != null) ? 1 : 0,
            'pph'=>($request->pph != null) ? 1 : 0,
            'user_id'=>Auth::user()->id
        ]);

        $jumlah = $request->qty * $request->harga_satuan;
        DetailTagihan::create([
            'invoice_tagihan'=>$invoice_new,
            'kode_unit'=>$request->kode_unit,
            'uraian'=>$request->uraian,
            'qty'=>$request->qty,
            'harga_satuan'=>$request->harga_satuan,
            'jumlah'=>$jumlah,
        ]);
        $ppn = $dt->saldo*(10/100);
        $pph = $dt->saldo*(2/100);
        Akun::where('nama_akun','PENDAPATAN USAHA')->update(['saldo'=>Akun::where('nama_akun','PENDAPATAN USAHA')->first()->saldo+ $jumlah]);
        Akun::where('nama_akun','UTANG PAJAK')->update(['saldo'=>Akun::where('nama_akun','UTANG PAJAK')->first()->saldo+$ppn]);
        Akun::where('nama_akun','PAJAK DIBAYAR DIMUKA')->update(['saldo'=>Akun::where('nama_akun','PAJAK DIBAYAR DIMUKA')->first()->saldo+$pph]);
        Akun::where('nama_akun','PIUTANG USAHA')->update(['saldo'=>Akun::where('nama_akun','PIUTANG USAHA')->first()->saldo+$dt->saldo+$ppn-$pph]);
        return redirect('tagihan/'.$invoice_new.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show($invoice)
    {
        $page = 'tagihan';
        $title = 'Tagihan';
        $costomers = Costomer::all();
        $units = Unit::all();
        $tagihan = Tagihan::find($invoice);
        return view('tagihan.view',compact('page','title','costomers','tagihan','units'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit($invoice)
    {
        $page = 'tagihan';
        $title = 'Tagihan';
        $costomers = Costomer::all();
        $tagihan = Tagihan::find($invoice);
        $units = Unit::all();
        return view('tagihan.edit',compact('page','title','costomers','tagihan','units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $invoice)
    {   

        if ($request->tombol == 'ubah') {
            Tagihan::find($invoice)->update([
                'tanggal'=>$request->tanggal,
                'no'=>$request->no,
                'site'=>$request->site,
                'id_costomer'=>$request->id_costomer,
                
                'ppn'=>($request->ppn != null) ? 1 : 0,
                'pph'=>($request->pph != null) ? 1 : 0,
                'user_id'=>Auth::user()->id
            ]);
            return redirect('tagihan');
        }
        else if($request->tombol == 'tambah_detail'){
            $jumlah = $request->qty * $request->harga_satuan;
            DetailTagihan::create([
                'invoice_tagihan'=>$invoice,
                'kode_unit'=>$request->kode_unit,
                'uraian'=>$request->uraian,
                'qty'=>$request->qty,
                'harga_satuan'=>$request->harga_satuan,
                'jumlah'=>$jumlah,
            ]);
            $dt = Tagihan::find($invoice);
            $ppn = $jumlah*(10/100);
            $pph = $jumlah*(2/100);
            Akun::where('nama_akun','PENDAPATAN USAHA')->update(['saldo'=>Akun::where('nama_akun','PENDAPATAN USAHA')->first()->saldo+ $jumlah]);
            Akun::where('nama_akun','UTANG PAJAK')->update(['saldo'=>Akun::where('nama_akun','UTANG PAJAK')->first()->saldo+$ppn]);
            Akun::where('nama_akun','PAJAK DIBAYAR DIMUKA')->update(['saldo'=>Akun::where('nama_akun','PAJAK DIBAYAR DIMUKA')->first()->saldo+$pph]);
            Akun::where('nama_akun','PIUTANG USAHA')->update(['saldo'=>Akun::where('nama_akun','PIUTANG USAHA')->first()->saldo+$jumlah+$ppn-$pph]);
            return redirect()->back();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy($invoice)
    {
        Tagihan::find($invoice)->delete();
        return redirect('tagihan');
    }
}
