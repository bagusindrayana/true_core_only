<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LevelUser;
class LevelUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'level';
        $title = 'Level Pengguna';
        $levelusers = LevelUser::all();
        return view('level.index',compact('page','title','levelusers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'level';
        $title = 'Level Pengguna';
       
        return view('level.create',compact('page','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd(($request->daftar_akun  != null) ? 1 : 0);
        LevelUser::create([
                'nama_level'=>$request->nama_level,
                'daftar_akun'=> ($request->daftar_akun  != null) ? 1 : 0 ,
                'data_kas'=>($request->data_kas != null) ? 1 : 0 ,
                'supplier'=> ($request->supplier != null) ? 1 : 0 ,
                'barang'=> ($request->barang != null) ? 1 : 0 ,
                'pemasukan'=>($request->pemasukan != null) ? 1 : 0 ,
                'pengeluaran'=> ($request->pengeluaran != null) ? 1 : 0 ,
                'transfer'=>($request->transfer != null) ? 1 : 0 ,
                'permintaan'=>($request->permintaan != null) ? 1 : 0 ,
                'pembelian'=> ($request->pembelian != null) ? 1 : 0 ,
                'transaksi_kas'=>($request->transaksi_kas != null) ? 1 : 0 ,
                'buku_besar'=>($request->buku_besar != null) ? 1 : 0 ,
                'neraca_saldo'=>($request->neraca_saldo != null) ? 1 : 0 ,
                'saldo_kas'=>($request->saldo_kas != null) ? 1 : 0 ,
                'jatuh_tempo'=> ($request->jatuh_tempo != null) ? 1 : 0 ,
                'laba_rugi'=> ($request->laba_rugi != null) ? 1 : 0 ,
                'profil'=>($request->profil != null) ? 1 : 0 ,
                'level_pengguna'=> ($request->level_pengguna != null) ? 1 : 0 ,
                'pengguna'=>($request->pengguna != null) ? 1 : 0 ,
                'tagihan'=>($request->tagihan != null) ? 1 : 0 ,
                'penerimaan'=> ($request->penerimaan != null) ? 1 : 0 ,
                'jurnal_umum'=>($request->jurnal_umum != null) ? 1 : 0 ,
                'costomer'=>($request->costomer != null) ? 1 : 0 ,
                'unit'=>($request->costomer != null) ? 1 : 0 ,
        ]);

        return redirect('level');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'level';
        $title = 'Level Pengguna';
        $leveluser = LevelUser::find($id);
        return view('level.edit',compact('page','title','leveluser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        LevelUser::where('id',$id)->update([
                'nama_level'=>$request->nama_level,
                'daftar_akun'=> ($request->daftar_akun  != null) ? 1 : 0 ,
                'data_kas'=>($request->data_kas != null) ? 1 : 0 ,
                'supplier'=> ($request->supplier != null) ? 1 : 0 ,
                'barang'=> ($request->barang != null) ? 1 : 0 ,
                'pemasukan'=>($request->pemasukan != null) ? 1 : 0 ,
                'pengeluaran'=> ($request->pengeluaran != null) ? 1 : 0 ,
                'transfer'=>($request->transfer != null) ? 1 : 0 ,
                'permintaan'=>($request->permintaan != null) ? 1 : 0 ,
                'pembelian'=> ($request->pembelian != null) ? 1 : 0 ,
                'transaksi_kas'=>($request->transaksi_kas != null) ? 1 : 0 ,
                'buku_besar'=>($request->buku_besar != null) ? 1 : 0 ,
                'neraca_saldo'=>($request->neraca_saldo != null) ? 1 : 0 ,
                'saldo_kas'=>($request->saldo_kas != null) ? 1 : 0 ,
                'jatuh_tempo'=> ($request->jatuh_tempo != null) ? 1 : 0 ,
                'laba_rugi'=> ($request->laba_rugi != null) ? 1 : 0 ,
                'profil'=>($request->profil != null) ? 1 : 0 ,
                'level_pengguna'=> ($request->level_pengguna != null) ? 1 : 0 ,
                'pengguna'=>($request->pengguna != null) ? 1 : 0 ,
                'tagihan'=>($request->tagihan != null) ? 1 : 0 ,
                'penerimaan'=> ($request->penerimaan != null) ? 1 : 0 ,
                'jurnal_umum'=>($request->jurnal_umum != null) ? 1 : 0 ,
                'costomer'=>($request->costomer != null) ? 1 : 0 ,
                'unit'=>($request->unit != null) ? 1 : 0 ,
        ]);

        return redirect('level');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LevelUser::where('id',$id)->delete();
        return redirect('level');
    }
}
