<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Costomer;
class CostomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $page = 'costomer';
        $costomers = Costomer::all();
        return view('costomer.index2',compact('page','costomers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'costomer';
       
        return view('costomer.create',compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Costomer::create([
            'nama_costomer'=>$request->nama_costomer,
            'alamat'=>$request->alamat,
            'no_telepon'=>$request->no_telepon
        ]);

        return redirect('costomer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'costomer';
        $costomer = Costomer::find($id);
        return view('costomer.edit',compact('page','costomer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Costomer::where('id',$id)->update([
            'nama_costomer'=>$request->nama_costomer,
            'alamat'=>$request->alamat,
            'no_telepon'=>$request->no_telepon
        ]);

        return redirect('costomer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Costomer::where('id',$id)->delete();

        return redirect('costomer');
    }
}
