<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Akun;
use App\Tagihan;
use App\Pemasukan;
use App\Pengeluaran;
use App\JurnalUmum;
class BukuBesarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'buku_besar';
        $title = 'Buku Besar';
        $akun = Akun::paginate(10);
        $tagihan = Tagihan::all();
        $pemasukan = Pemasukan::all();
        $pengeluaran = Pengeluaran::all();
        $jurnalumum = JurnalUmum::all();
        return view('buku_besar.index',compact('page','title','pemasukan','pengeluaran','jurnalumum','tagihan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = 'buku_besar';
        $title = 'Buku Besar';
        $akun = Akun::all();
        $from = $request->dari_tanggal;
        $to = $request->sampai_tanggal;
        $pemasukan = Pemasukan::whereBetween('tanggal',[$from,$to])->get();
        $pengeluaran = Pengeluaran::whereBetween('tanggal',[$from,$to])->get();
        $tagihan = Tagihan::whereBetween('tanggal',[$from,$to])->get();
        $jurnalumum = JurnalUmum::whereBetween('tanggal',[$from,$to])->get();
        return view('buku_besar.print',compact('page','title','pemasukan','pengeluaran','from','to','tagihan','jurnalumum'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
