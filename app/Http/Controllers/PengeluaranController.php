<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Pengeluaran;
use App\DetailPengeluaran;
use App\Akun;
use App\Unit;
class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'pengeluaran';
        $title = 'Pengeluaran';
     
        $pengeluarans = Pengeluaran::all();
        //dd($pengeluarans);
        return view('pengeluaran.index',compact('pengeluarans','page','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $page = 'pengeluaran';
        $title = 'Pengeluaran';
        $akuns = Akun::all();
        $units = Unit::all();

        return view('pengeluaran.create',compact('page','akuns','title','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
      
        $id = Pengeluaran::create([
            'tanggal'=>$request->tanggal,
            'id_akun_1'=>$request->id_akun1,
            'id_akun_2'=>$request->id_akun2,
            'untuk'=>$request->untuk,
            
            'user_id'=>Auth::user()->id,
            'nomor'=>$request->nomor
        ])->id;
        $jumlah = $request->qty * $request->harga_satuan;
        DetailPengeluaran::create([
            'id_pengeluaran'=>$id,
            'uraian'=>$request->uraian,
            'kode_unit'=>$request->kode_unit,
            'qty'=>$request->qty,
            'harga_satuan'=>$request->harga_satuan,
            'jumlah'=>$jumlah,
        ]);
        $a1 = Akun::findOrFail($request->id_akun1); //kredit
        $a1->update(['saldo'=>$a1->saldo-$jumlah]);
        
        $a2 = Akun::findOrFail($request->id_akun2); //debit
        $a2->update(['saldo'=>$a2->saldo+$jumlah]);
        return redirect('pengeluaran/'.$id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = 'pengeluaran';
        $title = 'Pengeluaran';
        $pengeluaran = Pengeluaran::find($id);
      
     
        return view('pengeluaran.view',compact('page','title','pengeluaran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'pengeluaran';
        $title = 'Pengeluaran';
        $pengeluaran = Pengeluaran::find($id);
        $akuns = Akun::all();
        $units = Unit::all();
        return view('pengeluaran.edit',compact('page','title','pengeluaran','akuns','units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        if ($request->tombol == 'ubah') {
            Pengeluaran::where('id',$id)->update([
                    'tanggal'=>$request->tanggal,
                    'id_akun_1'=>$request->id_akun1,
                    'id_akun_2'=>$request->id_akun2,
                    'untuk'=>$request->untuk,
                    
                    'user_id'=>Auth::user()->id,
                    'nomor'=>$request->nomor
                   
            ]);
            return redirect('pengeluaran');
        }
        else if($request->tombol == 'tambah_detail'){
            $jumlah = $request->qty * $request->harga_satuan;
            DetailPengeluaran::create([
                'id_pengeluaran'=>$id,
                'kode_unit'=>$request->kode_unit,
                'uraian'=>$request->uraian,
                'qty'=>$request->qty,
                'harga_satuan'=>$request->harga_satuan,
                'jumlah'=>$jumlah,
            ]);
            $pengeluaran = Pengeluaran::findOrFail($id);
            $a1 = Akun::findOrFail($pengeluaran->id_akun1);
            $a1->update(['saldo'=>$a1->saldo-$jumlah]);
            $a2 = Akun::findOrFail($pengeluaran->id_akun2);
            $a2->update(['saldo'=>$a2->saldo+$jumlah]);
            return redirect('pengeluaran/'.$id.'/edit');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pengeluaran::find($id)->delete();
        return redirect('pengeluaran');
    }
}
