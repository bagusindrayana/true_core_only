<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tagihan;
use App\Pemasukan;
use App\Pengeluaran;
use App\JurnalUmum;
class TransaksiKasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $page = 'transaksi_kas';
        $title = 'Transaksi Kas';
        $tagihan = Tagihan::all();
        $pemasukan = Pemasukan::all();
        $pengeluaran = Pengeluaran::all();
        $jurnalumum = JurnalUmum::all();
        return view('transaksi_kas.index',compact('page','title','tagihan','pemasukan','pengeluaran','jurnalumum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $from = date($request->dari_tanggal);
        $to = date($request->sampai_tanggal);
        $tagihan = Tagihan::whereBetween('tanggal',[$from,$to])->get();
        $pemasukan = Pemasukan::whereBetween('tanggal',[$from,$to])->get();
        $pengeluaran = Pengeluaran::whereBetween('tanggal',[$from,$to])->get();
        return view('transaksi_kas.print',compact('tagihan','pemasukan','pengeluaran'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
