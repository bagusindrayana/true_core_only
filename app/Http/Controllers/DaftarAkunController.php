<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Akun;
class DaftarAkunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $akuns = Akun::all();
    
        $page = 'akun';
        // return view('akun.index',compact('akuns','page'));
        return view('akun.index2',compact('akuns','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        
        $page = 'akun';
        $sub_akun = Akun::all();
        return view('akun.create',compact('page','sub_akun'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Akun::create([
            'nomor_akun'=>$request->nomor_akun,
            'nama_akun'=>$request->nama_akun,
            'jenis_akun'=>$request->jenis_akun,
            'tipe_akun'=>$request->tipe_akun,
            'kategori_akun'=>$request->kategori_akun,
            'sub_akun'=>$request->sub_akun,
            'tipe_saldo'=>$request->tipe_saldo,
            'saldo'=>$request->saldo,
            'saldo_awal'=>$request->saldo
        ]);

        return redirect('akun');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $akun = Akun::find($id);
        $sub_akun = Akun::whereNotIn('id',[$id])->get();
        $page = 'akun';
        return view('akun.edit',compact('page','akun','sub_akun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         Akun::where('id',$id)->update([
            'nomor_akun'=>$request->nomor_akun,
            'nama_akun'=>$request->nama_akun,
            'jenis_akun'=>$request->jenis_akun,
            'tipe_akun'=>$request->tipe_akun,
            'kategori_akun'=>$request->kategori_akun,
            'sub_akun'=>$request->sub_akun,
            'tipe_saldo'=>$request->tipe_saldo,
            'saldo'=>$request->saldo,
            'saldo_awal'=>$request->saldo
        ]);
        return redirect('akun');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $akun = Akun::where('id',$id)->delete();
        return redirect('akun');
    }
}
