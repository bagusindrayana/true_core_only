<?php

namespace App\Http\Controllers;
use App\Kas;
use Illuminate\Http\Request;

class DataKasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kas = Kas::all();
        $page = 'kas';
        return view('kas.index',compact('kas','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'kas';
        return view('kas.create',compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Kas::create(['nama_kas'=>$request->nama_kas,
                    'aktif'=>$request->aktif,
                    'penarikan'=>$request->penarikan,
                    'pemasukan'=>$request->pemasukan,
                    'transfer'=>$request->transfer
                    ]);
        return redirect('kas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kas = Kas::find($id);
        $page = 'kas';
        return view('kas.edit',compact('kas','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Kas::where('id',$id)->update(['nama_kas'=>$request->nama_kas,
                    'aktif'=>$request->aktif,
                    'penarikan'=>$request->penarikan,
                    'pemasukan'=>$request->pemasukan,
                    'transfer'=>$request->transfer
                    ]);
        return redirect('kas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kas::where('id',$id)->delete();
        return redirect('kas');
    }
}
