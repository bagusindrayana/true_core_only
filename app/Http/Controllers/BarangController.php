<?php

namespace App\Http\Controllers;

use Storage;
use App\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangs = Barang::all();
        $page = 'barang';
        return view('barang.index',compact('barangs','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $page = 'barang';
        return view('barang.create',compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $barang = new Barang;
        $kode = $barang->kode();
        $this->validate($request,[
            'gambar'=>'required|mimes:jpeg,jpg,png|max:5000',
            'nama_barang'=>'required|string',
            'merek_barang'=>'required|string',
        ]);
        $gambar = $kode.'.png';
        Barang::create([
                'kode_barang'=>$kode,
                'nama_barang'=>$request->nama_barang,
                'merek_barang'=>$request->merek_barang,
                'gambar_barang'=>$gambar
        ]);
        $request->file('gambar')->storeAs('barang',$gambar,'public_img');
        return redirect('barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $kode
     * @return \Illuminate\Http\Response
     */
    public function show($kode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $kode
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $barang = Barang::where('kode_barang',$kode)->first();
        $page = 'barang';
        return view('barang.edit',compact('barang','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $kode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {      

        $this->validate($request,[
     
            
            'nama_barang'=>'required|string',
            'merek_barang'=>'required|string'
        ]);
        $barang = Barang::where('kode_barang',$kode)->first();
        $exists = file_exists(public_path('images/barang/'.$barang->gambar_barang));
        $gambar = $barang->gambar_barang;
       
        
        if($exists && $request->file('gambar') != null){
            $this->validate($request,[
                'gambar'=>'required|mimes:jpeg,jpg,png|max:5000'
               
            ]);
            unlink(public_path('images/barang/'.$barang->gambar_barang));
            $gambar = $request->kode_barang.'.png';
            $request->file('gambar')->storeAs('barang',$gambar,'public_img');
        }

        if($barang->kode_barang != $request->kode_barang){
            $gambar = $request->kode_barang.'.png';
            rename(public_path('images/barang/'.$barang->gambar_barang), public_path('images/barang/'.$gambar));
        }
        Barang::where('kode_barang',$kode)->update([
                'kode_barang'=>$request->kode_barang,
                'nama_barang'=>$request->nama_barang,
                'merek_barang'=>$request->merek_barang,
                'gambar_barang'=>$gambar
        ]);
        return redirect('barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $kode
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {   
        $barang = Barang::where('kode_barang',$kode)->first();
        $exists = file_exists(public_path('images/barang/'.$barang->gambar_barang));
        if($exists){
            unlink(public_path('images/barang/'.$barang->gambar_barang));
        }
        Barang::where('kode_barang',$kode)->delete();
        return redirect('barang');
    }
}
