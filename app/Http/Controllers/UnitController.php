<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        $page = 'unit';
        $unit = Unit::all();
        return view('unit.index2', compact('page', 'unit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'unit';
        return view('unit.create',compact('unit','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_unit'=>'required|unique:units',
            'nama_unit'=>'required|string',
        ]);
        Unit::create([
                'kode_unit'=>$request->kode_unit,
                'nama_unit'=>$request->nama_unit,
        ]);
        return redirect('unit');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $kode
     * @return \Illuminate\Http\Response
     */
    public function show($kode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $kode
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $unit = Unit::where('kode_unit',$kode)->first();
        $page = 'unit';
        return view('unit.edit',compact('unit','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $kode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
        Unit::where('kode_unit',$kode)->update([
            'kode_unit'=>$request->kode_unit,
            'nama_unit'=>$request->nama_unit,
        ]);
        return redirect('unit?page='.$request->page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $kode
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        Unit::where('kode_unit',$kode)->delete();
        return redirect('unit');
    }
}
