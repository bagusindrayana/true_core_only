<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Akun;
use App\Kas;
use App\Supplier;
use App\Barang;
use App\Costomer;
use App\Unit;
use App\Tagihan;
use App\Pemasukan;
use App\Pengeluaran;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $users = User::all();
        $akuns = Akun::get()->count();
        $kas = Kas::get()->count();
        $suppliers = Supplier::get()->count();
        $barangs = Barang::get()->count();
        $costomers = Costomer::get()->count();
        $units = Unit::get()->count();
        $tagihans = Tagihan::get()->count();
        $pengeluarans = Pengeluaran::get()->count();
        $pemasukans = Pemasukan::get()->count();
        return view('home',compact('users','akuns','kas','suppliers','barangs','costomers','units','tagihans','pengeluarans','pemasukans'));
    }

    public function printData($page,$id){
        
            return view($page.'.print',compact('id'));
        
    }
}
