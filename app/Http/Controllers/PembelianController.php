<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permintaan;
use App\DetailPermintaan;
use App\Pembelian;
use App\DetailPembelian;
use App\User;
use App\Supplier;
use App\Gudang;
use App\Helpers\TrueHelper;
class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'pembelian';
        $title = 'Pembelian';
        $pembelians = Pembelian::all();
        return view('pembelian.index',compact('page','title','pembelians'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'pembelian';
        $title = 'Pembelian';
        $user = User::all();
        $permintaan = Permintaan::where('status',1)->get();
        return view('pembelian.create',compact('page','title','user','permintaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pembelian = new Pembelian;
        $kode = $pembelian->kode();

        $id = Pembelian::create([
            'nomor' => $kode,
            'tanggal' => $request->tanggal,
            'permintaan_id' => $request->permintaan_id,
            'user_id' => $request->user_id
        ])->id;

        // Permintaan::where('id',$request->permintaan_id)->update(['status'=>1]);

        $d = DetailPermintaan::where('permintaan_id',$request->permintaan_id)->get();
        foreach ($d as $dd) {

            // $harga = TrueHelper::harga_dasar($dd->kode_barang);

            DetailPembelian::create([
                'pembelian_id' => $id,
                'permintaan_id' => $dd->permintaan_id,
                'kode_barang' => $dd->kode_barang,
                'kode_unit' => $dd->kode_unit,
                'jumlah' => $dd->jumlah,
                'harga' => TrueHelper::harga_dasar($dd->kode_barang),
                'uraian' => $dd->uraian
            ]);

            // if(Gudang::where('kode_barang',$dd->kode_barang)->count() > 0){
            //     Gudang::where('kode_barang',$dd->kode_barang)->update([
               
            //         'jumlah'=>$dd->jumlah,
            //         'harga'=>$dd->harga
            //     ]); 
            // }
            // else {
            //    Gudang::create([
            //         'kode_barang'=>$dd->kode_barang,
            //         'jumlah'=>$dd->jumlah,
            //         'harga'=>$dd->harga
            //     ]); 
            // }
                
        }
        return redirect('pembelian/'.$id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = 'pembelian';
        $title = 'Pembelian';
        $user = User::all();
        $permintaan = Permintaan::all();
        $pembelian = DetailPembelian::where('pembelian_id',$id)->get();
        $data = Pembelian::findOrFail($id);
        $supplier = Supplier::all();
        return view('pembelian.view',compact('page','title','user','permintaan','pembelian','data','supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = 'pembelian';
        $title = 'Pembelian';
        $user = User::all();
        $permintaan = Permintaan::all();
        $pembelian = DetailPembelian::where('pembelian_id',$id)->get();
        $data = Pembelian::findOrFail($id);
        $supplier = Supplier::all();
        return view('pembelian.edit',compact('page','title','user','permintaan','pembelian','data','supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {	
        $input = $request->all();
        unset($input['_token']);
        unset($input['kode_barang']);
        unset($input['_method']);
        unset($input['harga']);
        unset($input['supplier_id']);
        unset($input['jumlah']);
        unset($input['example0_length']);
        unset($input['id_detail']);
        Pembelian::findOrFail($id)->update($input);
        Pembelian::where('id',$id)->update(['status'=>1]);
        for ($i=0; $i < count($request->id_detail); $i++) 
        { 
        	DetailPembelian::findOrFail($request->id_detail[$i])->update(['jumlah'=>$request->jumlah[$i],'harga'=>$request->harga[$i],'supplier_id'=>$request->supplier_id[$i]]);

            Gudang::create([
                'pembelian_id'=>$id,
                'kode_barang'=>$request->kode_barang[$i],
                'jumlah'=>$request->jumlah[$i],
                'harga'=>$request->harga[$i]
            ]); 
        }
        return redirect('pembelian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pembelian::find($id)->delete();
        return redirect()->back();
    }
}
