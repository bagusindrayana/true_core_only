<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPemakaian extends Model
{
    protected $guarded = [];

    public function barang(){
    	return $this->belongsTo(Barang::class,'kode_barang','kode_barang');
    }

    public function unit(){
    	return $this->belongsTo(Unit::class,'kode_unit','kode_unit');
    }
}
