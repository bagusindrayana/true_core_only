<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pajak extends Model
{
    protected $guarded = [];


    public function akun_debit($value='')
    {
    	return $this->belongsTo(Akun::class,'akun_debit_id');
    }

    public function akun_kredit($value='')
    {
    	return $this->belongsTo(Akun::class,'akun_kredit_id');
    }

    public function akun_pajak_debit($value='')
    {
    	return $this->belongsTo(Akun::class,'akun_pajak_debit_id');
    }

    public function akun_pajak_kredit($value='')
    {
    	return $this->belongsTo(Akun::class,'akun_pajak_kredit_id');
    }

    public function transaksi($jenis_transaksi){
    	$jenis = "";
    	if($jenis_transaksi == 1){
    		$jenis = "Tagihan";
    	}
    	if($jenis_transaksi == 2){
    		$jenis = "Pemasukan";
    	}
    	if($jenis_transaksi == 3){
    		$jenis = "Pengeluaran";
    	}

    	return $jenis;
    }
}
