<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurnalUmum extends Model
{
    protected $guarded = [];

    public function detail(){
    	return $this->hasMany('App\DetailJurnalUmum','id_jurnal_umum','id');
    }



    public function delete(){
    	$this->detail()->delete();
    	parent::delete();
    }
}
