<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    protected $guarded = [];

	public function user(){
		return $this->belongsTo('App\User','user_id');
	}

	public function akun1(){
		return $this->belongsTo('App\Akun','id_akun_1','id');
	}

	public function akun2(){
		return $this->belongsTo('App\Akun','id_akun_2','id');
	}
	

	
	public function detail(){
    	return $this->hasMany('App\DetailPengeluaran','id_pengeluaran','id');
    }

    public function delete(){
    	$this->detail()->delete();
    	parent::delete();
    }

    public function getSaldoAttribute (){
    	$saldo = 0;
    	foreach ($this->detail as $d) {
    		$saldo += $d->jumlah;
    	}
    	return $saldo;
    }

}
