<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tagihan extends Model
{
    protected $primaryKey = "invoice";
    public $incrementing = false;
    protected $guarded = [];

    public function costomer(){
		return $this->belongsTo('App\Costomer','id_costomer','id');
	}
	public function user(){
		return $this->belongsTo('App\User','user_id');
	}

    

	public function detail(){
    	return $this->hasMany('App\DetailTagihan','invoice_tagihan','invoice');
    }

    public function delete(){
    	$this->detail()->delete();
    	parent::delete();
    }

    public function getSaldoAttribute (){
        $saldo = 0;
        foreach ($this->detail as $d) {
            $saldo += $d->jumlah;
        }
        return $saldo;
    }

   
}
