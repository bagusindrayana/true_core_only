<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','level_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function pemasukan(){
        return $this->hasMany('App\Pemasukan','user_id');
    }

    public function tagihan(){
        return $this->hasMany('App\Tagihan','user_id');
    }

    public function level_user(){
        return $this->belongsTo('App\LevelUser','level_user_id');
    }

    


}
