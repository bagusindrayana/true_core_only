@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Daftar Akun</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Daftar Akuns</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Daftar Akun
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nomor_akun" class="form-label" >Nomor Akun</label>
                            <input type="text" class="form-control" id="nomor_akun" name="nomor_akun"   placeholder="Nomor Akun">
                        </div>

                        <div class="form-group">
                            <label for="nama_akun" class="form-label">Nama Akun</label>
                            <input type="text" class="form-control" id="nama_akun" name="nama_akun"  placeholder="Nama Akun">
                        </div>

                    

                        <div class="form-group">
                            <label for="jenis_akun" class="form-label">Jenis Akun</label>
                            <select class="form-control" id="jenis_akun" name="jenis_akun">
                                <option>Pilih Jenis Akun</option>
                                <option value="1">Aktiva</option>
                                <option value="2">Pasiva</option>
                                <option value="3">Ekuitas</option>
                                <option value="4">Pendapatan</option>
                                <option value="5">Biaya-Biaya</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tipe_akun" class="form-label">Tipe Akun</label>
                            <select class="form-control" id="tipe_akun" name="tipe_akun">
                                <option>Pilih Tipe Akun</option>
                                <option value="1">Cash/Bank</option>
                                <option value="2">Non Cash/Bank</option>
                               
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="kategori_akun" class="form-label">Kategori Akun</label>
                            <select class="form-control" id="kategori_akun" name="kategori_akun">
                                <option>Pilih Kategori Akun</option>
                                <option value="1">General</option>
                                <option value="2">Detail</option>
                               
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="sub_akun"  class="form-label" >Sub Akun</label>
                            <select class="form-control" id="sub_akun" name="sub_akun">
                               
                                <option value="0">Tidak Ada</option>
                                @foreach($sub_akun as $akun)
                                    @if($akun->sub_akun == 0)
                                        <option value="{{ $akun->id }}">{{ $akun->nomor_akun }} | {{ $akun->nama_akun }}</option>
                                        @foreach($sub_akun as $sub_sub_akun)
                                            @if($sub_sub_akun->sub_akun != 0 && $sub_sub_akun->sub_akun == $akun->id)
                                                <option value="{{ $sub_sub_akun->id }}">&nbsp;&nbsp;&nbsp;&nbsp;{{ $sub_sub_akun->nomor_akun }} | {{ $sub_sub_akun->nama_akun }}</option>
                                                @foreach($sub_akun as $sub_sub_sub_akun)
                                                    @if($sub_sub_sub_akun->sub_akun != 0 && $sub_sub_sub_akun->sub_akun == $sub_sub_akun->id)
                                                        <option value="{{ $sub_sub_sub_akun->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $sub_sub_sub_akun->nomor_akun }} | {{ $sub_sub_sub_akun->nama_akun }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                            
                                        @endforeach
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tipe_saldo" class="form-label">Tipe Saldo</label>
                            <select class="form-control" id="tipe_saldo" name="tipe_saldo">
                                <option>Pilih Tipe Saldo</option>
                                <option value="1">Debit</option>
                                <option value="2">Kredit</option>
                            </select>
                        </div>

                    

                        <div class="form-group">
                            <label for="saldo" class="form-label">Saldo (Rp.)</label>
                            <input type="number" class="form-control" id="saldo" name="saldo"  min="0" value="0">
                        </div>
                       
                        
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection