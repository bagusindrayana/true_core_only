@extends('layouts.template')

@section('content')
<?php use App\Akun; ?>
<div class="page-header">
    <h1 class="title">Daftar Akun</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active">Daftar Akun</li>
        <li style="float: right;"><a href="{{ url($page.'/create') }}" class="btn btn-default">Tambah Data</a> </li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"></div>
                <div class="panel-body table-responsive">
                   
                    <table  class="table display">
                        <thead>
                            <tr>
                               
                                <th>Nomor Akun</th>
                                <th>Nama Akun</th>
                                
                                <th>Tipe Akun</th>
                               
                                <th>Tipe Saldo</th>
                                <th>Saldo</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($akuns as $data)
                                @if($data->sub_akun == 0)
                                    <tr >
                                       
                                        <td>{{ $data->nomor_akun }}</td>
                                        <td>{{ $data->nama_akun }}</td>
                                      
                                        
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="aksi">
                                            <a href="{{ url($page.'/'. $data->id.'/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                         document.getElementById('delete<?php echo $data->id;  ?>').submit();" class="fa fa-trash"></a></td>
                                            <form action="{{ url($page.'/'.$data->id) }}" id="delete<?php echo $data->id;  ?>" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE">
                                            </form>
                                    </tr>
                                    
                                    @foreach($akuns as $sub_data)
                                        @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                            <tr>
                                                
                                                <td style="padding-left: 5%;">{{ $sub_data->nomor_akun }}</td>
                                                <td>{{ $sub_data->nama_akun }}</td>
                                                
                                                <td>@if($sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                
                                                <td>@if($sub_data->tipe_saldo == 1) Debit @elseif($sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                <td>Rp.{{ number_format($sub_data->saldo,2) }}</td>
                                                <td class="aksi">
                                                    <a href="{{ url($page.'/'. $sub_data->id.'/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                                 document.getElementById('delete<?php echo $sub_data->id;  ?>').submit();" class="fa fa-trash"></a></td>
                                                    <form action="{{ url($page.'/'.$sub_data->id) }}" id="delete<?php echo $sub_data->id;  ?>" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                    </form>
                                            </tr>
                                            @foreach($akuns as $sub_sub_data)
                                                @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                                    <tr>
                                                        
                                                        <td style="padding-left: 10%;">{{ $sub_sub_data->nomor_akun }}</td>
                                                        <td>{{ $sub_sub_data->nama_akun }}</td>
                                                        
                                                        <td>@if($sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                        
                                                        <td>@if($sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                        <td>Rp.{{ number_format($sub_sub_data->saldo,2) }}</td>
                                                        <td class="aksi">
                                                            <a href="{{ url($page.'/'. $sub_sub_data->id.'/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                                         document.getElementById('delete<?php echo $sub_sub_data->id;  ?>').submit();" class="fa fa-trash"></a></td>
                                                            <form action="{{ url($page.'/'.$sub_sub_data->id) }}" id="delete<?php echo $sub_sub_data->id;  ?>" method="POST">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="_method" value="DELETE">
                                                            </form>
                                                    </tr>
                                                    @foreach($akuns as $sub_sub_sub_data)
                                                        @if($sub_sub_sub_data->sub_akun == $sub_sub_data->id && $sub_sub_sub_data->sub_akun != 0)
                                                            <tr>
                                                                
                                                                <td style="padding-left: 10%;">{{ $sub_sub_sub_data->nomor_akun }}</td>
                                                                <td>{{ $sub_sub_sub_data->nama_akun }}</td>
                                                                
                                                                <td>@if($sub_sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                                
                                                                <td>@if($sub_sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                                <td>Rp.{{ number_format($sub_sub_sub_data->saldo,2) }}</td>
                                                                <td class="aksi">
                                                                    <a href="{{ url($page.'/'. $sub_sub_sub_data->id.'/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                                                 document.getElementById('delete<?php echo $sub_sub_sub_data->id;  ?>').submit();" class="fa fa-trash"></a></td>
                                                                    <form action="{{ url($page.'/'.$sub_sub_sub_data->id) }}" id="delete<?php echo $sub_sub_sub_data->id;  ?>" method="POST">
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                    </form>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                        
                                    
                                
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection