@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Kas</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active">Data Kas</li>
        <li style="float: right;"><a href="{{ url($page.'/create') }}" class="btn btn-default">Tambah Data</a> </li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"> </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Kas</th>
                                <th>Aktif</th>
                                <th>Penarikan</th>
                                <th>Pemasukan</th>
                                <th>Transfer</th>
                               
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kas as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->nama_kas }}</td>
                                    <td>@if($data->aktif == 1) Iya @else Tidak @endif</td>
                                    <td>@if($data->penarikan == 1) Iya @else Tidak @endif</td>
                                    <td>@if($data->pemasukan == 1) Iya @else Tidak @endif</td>
                                    <td>@if($data->transfer == 1) Iya @else Tidak @endif</td>
                                    
                                    <td class="aksi">
                                        <a href="{{ url($page.'/'. $data->id.'/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('edit').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url($page.'/'.$data->id) }}" id="edit" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection