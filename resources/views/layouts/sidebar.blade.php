<div class="sidebar clearfix">
            <ul class="sidebar-panel nav metismenu" id="side-menu" data-plugin="metismenu">
                <li class="active"><a href="{{ url('home') }}"><span class="icon color5"><i class="fa fa-home" aria-hidden="true"></i></span><span class="nav-title">Dashboard</span> </a>
                    
                </li>
                <!-- <li class="sidetitle">FEATURES</li>   -->      
               @if(Auth::user()->level_user->daftar_akun == 1 || Auth::user()->level_user->customer == 1 || Auth::user()->level_user->supplier == 1 || Auth::user()->level_user->barang == 1 || Auth::user()->level_user->unit == 1)        
                <li><a href="#"><span class="icon color7"><i class="fa fa-archive" aria-hidden="true"></i></span>Master Data<i class="fa fa-sort-desc caret"></i></a>
                    <ul>
                        @if(Auth::user()->level_user->daftar_akun == 1)
                        <li><a href="{{ url('akun') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Daftar Akun</a></li>
                        @endif
                        
                        @if(Auth::user()->level_user->supplier == 1)
                        <li><a href="{{ url('supplier') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Supplier</a></li>
                        @endif

                        @if(Auth::user()->level_user->costomer == 1)
                        <li><a href="{{ url('costomer') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Costomer</a></li>
                        @endif

                        @if(Auth::user()->level_user->barang == 1)
                        <li><a href="{{ url('barang') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Barang</a></li>
                        @endif

                        @if(Auth::user()->level_user->unit == 1)
                        <li><a href="{{ url('unit') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Unit</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if(Auth::user()->level_user->pemasukan == 1 || Auth::user()->level_user->pemasukan == 1 || Auth::user()->level_user->tagihan == 1 || Auth::user()->level_user->jurnal_umum == 1)
                    <li><a href="#"><span class="icon color9"><i class="fa fa-money" aria-hidden="true"></i></span>Transaksi <i class="fa fa-sort-desc caret"></i></a>
                        <ul>
                            @if(Auth::user()->level_user->tagihan == 1)
                            <li><a href="{{ url('tagihan') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Tagihan</a></li>
                            @endif

                            @if(Auth::user()->level_user->pemasukan == 1)
                            <li><a href="{{ url('pemasukan') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Pemasukan</a></li>
                            @endif
                            @if(Auth::user()->level_user->pengeluaran == 1)
                            <li><a href="{{ url('pengeluaran') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Pengeluaran</a></li>
                            @endif
                            @if(Auth::user()->level_user->jurnal_umum == 1)
                            <li><a href="{{ url('jurnalumum') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Jurnal Umum</a></li>
                            @endif

                            
                        </ul>
                    </li>
                @endif

                @if(Auth::user()->level_user->permintaan == 1 || Auth::user()->level_user->pembelian == 1 || Auth::user()->level_user->pemakaian == 1 )
                <li><a href="#"><span class="icon color9"><i class="fa fa-th" aria-hidden="true"></i></span>Inventory <i class="fa fa-sort-desc caret"></i></a>
                    <ul>
                        @if(Auth::user()->level_user->permintaan == 1)
                            <li><a href="{{ url('permintaan') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Permintaan</a></li>
                        @endif

                        @if(Auth::user()->level_user->pembelian == 1)
                            <li><a href="{{ url('pembelian') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Pembelian</a></li>
                        @endif
                        @if(Auth::user()->level_user->pemakaian == 1)
                            <li><a href="{{ url('pemakaian') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Pemakaian</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if(Auth::user()->level_user->transaksi_kas == 1 || Auth::user()->level_user->buku_besar  == 1 ||  Auth::user()->level_user->neraca_saldo == 1 || Auth::user()->level_user->saldo_kas == 1 || Auth::user()->level_user->jatuh_tempo == 1 || Auth::user()->level_user->laba_rugi == 1)
                <li><a href="#"><span class="icon color10"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Laporan <i class="fa fa-sort-desc caret"></i></a>
                    <ul>
                        @if(Auth::user()->level_user->transaksi_kas == 1)
                        <li><a href="{{ url('transaksi_kas') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Jurnal</a></li>
                        @endif

                        @if(Auth::user()->level_user->buku_besar  == 1)
                        <li><a href="{{ url('buku_besar') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Buku Besar</a></li>
                        @endif

                        @if(Auth::user()->level_user->laba_rugi  == 1)
                        <li><a href="{{ url('laba_rugi') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Laba Rugi</a></li>
                        @endif

                        @if(Auth::user()->level_user->neraca_saldo  == 1)
                        <li><a href="{{ url('neraca_saldo') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Neraca Saldo</a></li>
                        @endif
                        
                        

                        @if(Auth::user()->level_user->laba_rugi  == 1)
                        <li><a href="{{ url('gudang') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Gudang</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @if(Auth::user()->level_user->pengguna == 1 || Auth::user()->level_user->profil == 1 || Auth::user()->level_user->level_pengguna == 1)
                <li><a href="#"><span class="icon color10"><i class="fa fa-gear" aria-hidden="true"></i></span>Pengaturan <i class="fa fa-sort-desc caret"></i></a>
                    <ul>
                        @if(Auth::user()->level_user->profil == 1)
                            <li><a href="{{ url('profil') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Profil</a></li>
                        @endif
                        @if(Auth::user()->level_user->level_pengguna == 1)
                        <li><a href="{{ url('level') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Level Pengguna</a></li>
                        @endif
                        @if(Auth::user()->level_user->pengguna == 1)
                        <li><a href="{{ url('user') }}"><i class="fa fa-angle-right" aria-hidden="true"></i> Pengguna</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                
            </ul>
        </div>