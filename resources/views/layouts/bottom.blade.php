<!-- jQuery Library --> 
<script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script> 
<!-- Bootstrap Core JavaScript File --> 
<script src="{{ url('js/bootstrap/tether.min.js') }}"></script>
<script src="{{ url('js/bootstrap/bootstrap.min.js') }}"></script> 


<!-- Plugin.js - Some Specific JS codes for Plugin Settings --> 
<script type="text/javascript" src="{{ url('js/plugins.js') }}"></script>


<script type="text/javascript" src="{{ url('js/moment/moment.min.js') }}"></script> 

<script type="text/javascript" src="{{ url('js/date-range-picker/daterangepicker.js') }}"></script>

<!-- MetisMenu --> 
<script type="text/javascript" src="{{ url('js/metismenu/metisMenu.min.js') }}"></script>
<!-- Flot Chart --> 
<script type="text/javascript" src="{{ url('js/flot-chart/flot-chart.js') }}"></script><!-- main file --> 
<script type="text/javascript" src="{{ url('js/flot-chart/flot-chart-time.js') }}"></script><!-- time.js --> 
<script type="text/javascript" src="{{ url('js/flot-chart/flot-chart-stack.js') }}"></script><!-- stack.js --> 
<script type="text/javascript" src="{{ url('js/flot-chart/flot-chart-pie.js') }}"></script><!-- pie.js --> 
<script type="text/javascript" src="{{ url('js/flot-chart/flot-chart-plugin.js') }}"></script><!-- demo codes --> 
<!-- Chartist --> 
<script type="text/javascript" src="{{ url('js/chartist/chartist.js') }}"></script><!-- main file --> 
<script type="text/javascript" src="{{ url('js/chartist/chartist-plugin.js') }}"></script><!-- demo codes --> 
<!-- Counterup -->
<script type="text/javascript" src="{{ url('js/counterup/jquery.waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/counterup/jquery.counterup.min.js') }}"></script>

<script src="{{ url('js/datatables/datatables.min.js') }}"></script>

<!-- JQGrid Table -->
<!-- <script src="{{ url('js/jqgrid/grid.locale-en.js') }}"></script>
<script src="{{ url('js/jqgrid/jqgrid-plugins.js') }}"></script>
<script src="{{ url('js/jquery.jqGrid.min.js') }}"></script> -->

<script type='text/javascript' src="{{ url('jquery.sumtr.js') }}"></script>

<script type="text/javascript">
	$('#date-picker').daterangepicker({
            singleDatePicker: true
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
	function pilih_data(id,target){
		$(target).val(id);
	}

	
	
	var target_id = '';
	function pilih_target(tg){

		target_id = tg;
		
	}



	function pilih_data_2(id,target){
		
		$(target_id).val(id);
		
	}
	var i = 1;
	if($('.listnya')){
		i = $('.listnya').length;
	}


	function add_item(){
		i++;
		var klon = $("#list-1").clone().appendTo('#detail_jurnal');
        klon.attr('id', 'list-'+i);
        $("#list-"+i+" .pilih-akun").val(0);
        $("#list-"+i+" .input_debit").val(0);

       	$("#list-"+i+" .input_kredit").val(0);
       	$("#list-"+i+" .del_list").attr("list-id","list-"+i);
       	$("#list-"+i+" .del_list").show();
       	$("#list-"+i+" .uraian").val('');

        klon.show();
        
        
	}

	
	function cek_ofb(){
		$("#ofb").val(parseInt($("#total_debit").val())-parseInt($("#total_kredit").val()))
	}

	function hitung_debit(){
		var total = 0;
    	$(".input_debit").each(function(){
		 	var input = $(this);
		 	var d = parseInt(input.val());
		 	if(!(d+d)){
	    		d = 0;
	    	}

	    	total += d;
		});
    	
    	$("#total_debit").val(parseInt(total));
	}

	function hitung_kredit(){
		var total = 0;
    	$(".input_kredit").each(function(){
		 var input = $(this);
		 	var d = parseInt(input.val());
		 	if(!(d+d)){
	    		d = 0;
	    	}

	    	total += d;
		});
    	
    	$("#total_kredit").val(parseInt(total));
	}



	
	$(document).on('click', '#add_item', function(event) {
      	add_item();
    });

    $(document).on('keyup', '.input_debit', function(event) {
    	hitung_debit();
    	cek_ofb();
    });

    $(document).on('keyup', '.input_kredit', function(event) {
    	hitung_kredit();
    	cek_ofb();
    });

    $(document).on('submit', '#form_jurnal', function(event) {
    	if(parseInt($("#ofb").val()) == 0){

    		return true;
    	}
    	else {
    		alert("Out Of Balance tidak sesuai");
    		return false;
    	}
    });


    $(document).on('click', '.del_list', function(event) {
    	var id_list = $(this).attr("list-id");

    	$("#"+id_list).remove();
    	hitung_debit();
    	hitung_kredit();
    	cek_ofb();

    });



</script>
</body>
</html>