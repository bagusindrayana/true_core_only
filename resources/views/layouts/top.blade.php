<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>PT.True</title>
    <!-- Css Files -->

    <link href="{{ url('css/root.css') }}" rel="stylesheet">

</head>

<?php use App\Profil;
    $profilnya = Profil::first();
 ?>
<body>
    <style type="text/css">
        .del_list:hover {
            cursor: pointer;
        }
        .aksi {
            font-size: 20px;
        }
        .form-label,.form-control {
            float: left;
            margin-bottom: 10px;
        }

        .form-label {
            width: 40%;
           
            margin-top: 10px;
        }

        .pagination {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            padding-left: 0;
            list-style: none;
            border-radius: .25rem;
        }
        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 22px 0;
            border-radius: 4px;
        }

        .pagination li {
            float: left;
            padding: 10px;

        }

        .pagination .active {
            background:#89F1C1;
            color: white;
        }


        fieldset {
            width: 60%;
            float: right;
            margin-bottom: 10px;
        }       

        .form-control {
            width: 60%;
        }

        .display {
            margin: 0;
            font-size: 10px;
        }

        @media only screen and (max-width: 768px) {
            /* For mobile phones: */
            .form-label,fieldset,.form-control {
                width: 100%;
                margin-top: 0;
            }
        }

        
    </style>
    <style type="text/css">
    #testleft {
        display: inline;
        text-align: left
    }

    #testright {
        display: inline;
        text-align: right;
    }
</style>
    <!-- Start Page Loading -->
    <div class="loading"><img src="{{ url('img/loading.gif') }}" alt="loading-img"></div>
    <!-- End Page Loading --> 
    <!-- Start Top -->
    <div id="top" class="clearfix"> 
        <!-- Start App Logo -->
        <div class="applogo"> <a href="{{ url('home') }}" class="logo">{{ $profilnya->nama }}</a> </div>
        <!-- End App Logo --> 
        <!-- Start Sidebar Show Hide Button --> 
        <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a> <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a> 
        <!-- End Sidebar Show Hide Button --> 
        <!-- Start Searchbox -->
        <form class="searchform">
            <input type="text" class="searchbox" id="searchbox" placeholder="Cari Sesuatu...">
            <span class="searchbutton"><i class="fa fa-search"></i></span>
        </form>
        <!-- End Searchbox --> 
        <!-- Start Sidepanel Show-Hide Button --> 
        <a href="#sidepanel" class="sidepanel-open-button"><i class="fa fa-outdent"></i></a> 
        <!-- End Sidepanel Show-Hide Button --> 
        <!-- Start Top Right -->
        <ul class="top-right">
            <li class="dropdown dropdown-notification nav-item link"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label count-info"><span class="label label-warning">6</span><i class="fa fa-envelope" aria-hidden="true"></i></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right top-width">
                    <li class="dropdown-menu-header">
                        <h6 class="dropdown-header"><span>Notifications</span><span class="pull-right label label-danger">6 New</span></h6>
                    </li>
                    <li class="list-group"><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading color10"><i class="fa fa-shopping-cart"></i> You have new order!</h6>
                                <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading darken-1 color7"><i class="fa fa-desktop"></i> 99% Server load</h6>
                                <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading color9"><i class="fa fa-server"></i> Warning notifixation</h6>
                                <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading color11"><i class="fa fa-check"></i> Complete the task</h6>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading"><i class="fa fa-bar-chart"></i> Generate monthly report</h6>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a></li>
                        <li class="dropdown-menu-footer"><a href="#" class="dropdown-item text-muted text-center">Read all notifications</a></li>
                    </ul>
                </li>
                <li class="dropdown link"> <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><b>{{ Auth::user()->name }}</b><span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
                        <li role="presentation" class="dropdown-header">Profile</li>
                        
                        
                        <li><a href="#"><i class="fa falist fa-wrench"></i>Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa falist fa-lock"></i> Lockscreen</a></li>
                        <li onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><a href="#"><i class="fa falist fa-power-off"></i> Logout</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>
            </ul>
            <!-- End Top Right --> 
        </div>