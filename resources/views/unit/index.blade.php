@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Unit</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active">Data Unit</li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"><a href="{{ url($page.'/create') }}" class="btn btn-default">Tambah Data</a> </div>
                
                <div class="panel-body table-responsive">
                    <table class="table display">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode Unit</th>
                                <th>Nama Unit</th>
                               
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>T-001</td>
                                <td>TEST1</td>
                                
                                <td class="aksi">
                                    <a href="{{ url($page.'/1/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                 document.getElementById('delete1').submit();" class="fa fa-trash"></a>
                                </td>
                                <form action="{{ url($page.'/1') }}" id="delete1" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                </form>
                            </tr> 
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection