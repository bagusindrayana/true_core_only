@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Unit</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data Unit</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title">
                    Tambah Data Unit
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="kode_unit" class="form-label">Kode Unit</label>
                            <input required type="text" class="form-control" id="kode_unit" name="kode_unit">
                        </div>

                        <div class="form-group">
                            <label for="nama_unit" class="form-label">Nama Unit</label>
                            <input required type="text" class="form-control" id="nama_unit" name="nama unit">
                        </div>
                        
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection