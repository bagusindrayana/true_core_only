 <?php 
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition:attachment;filename=laporan_laba_rugi_".date('F')."-".date("Y_m_d_h_i_s").".xls"); 

 ?>

 <table >
     <tr  style="text-align: center;font-weight: bold;font-size: 20px;">
         <td colspan="5" >PT. TRAS RENTAL UTAMA EKAMULYA</td>
     </tr>
     <tr style="text-align: center;font-weight: bold;font-size: 20px;">
         <td colspan="5">Laporan Laba Rugi</td>
     </tr style="text-align: center;font-weight: bold;font-size: 20px;">

     <tr style="text-align: center;font-weight: bold;font-size: 20px;">
         <td colspan="5">Periode {{ $bulan }}</td>
     </tr>
     <tr>
         <td colspan="5"><br><br><br><br></td>
     </tr>
 </table>
        <table  border="1">
            <thead>
                <tr>
                    <th>Nomor Akun</th>
                    <th>Nama Akun</th>     
                    <th>Tipe Akun</th>
                    <th>Tipe Saldo</th>
                    <th>Saldo</th>
                  
                </tr>
            </thead>
            <tbody>
                @foreach($akuns as $data)
                    @if($data->sub_akun == 0 && $data->jenis_akun == 4)
                        <tr >
                           
                            <td>{{ $data->nomor_akun }}</td>
                            <td>{{ $data->nama_akun }}</td>
                            
                            <td></td>
                            <td></td>
                            <td></td>
                        
                        </tr>
                        <?php 
                            $pendapatan = 0;
                            $biaya = 0;
                         ?>
                        @foreach($akuns as $sub_data)
                            @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                <tr>
                                    <?php $pendapatan += (int)$sub_data->saldo; ?>
                                    <td >{{ $sub_data->nomor_akun }}</td>
                                    <td >{{ $sub_data->nama_akun }}</td>
                                    
                                    <td>@if($sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                    
                                    <td>@if($sub_data->tipe_saldo == 1) Debit @elseif($sub_data->tipe_saldo == 2) Kredit @endif</td>
                                    <td>
                                        Rp.{{ number_format($sub_data->saldo) }}
                                    </td>
                                  
                                </tr>
                                @foreach($akuns as $sub_sub_data)
                                    @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                        <tr>
                                            <?php $pendapatan += (int)$sub_sub_data->saldo; ?>
                                            <td >{{ $sub_sub_data->nomor_akun }}</td>

                                            <td >{{ $sub_sub_data->nama_akun }}</td>
                                            
                                            <td>@if($sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                            
                                            <td>@if($sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                            <td>
                                                <div style="text-align: right;">Rp. {{ number_format($sub_sub_data->saldo) }}</div>
                                            </td>
                                          
                                        </tr>
                                        @foreach($akuns as $sub_sub_sub_data)
                                            @if($sub_sub_sub_data->sub_akun == $sub_sub_data->id && $sub_sub_sub_data->sub_akun != 0)
                                                <tr>
                                                    
                                                    <td >{{ $sub_sub_sub_data->nomor_akun }}</td>

                                                    <td >{{ $sub_sub_sub_data->nama_akun }}</td>
                                                    
                                                    <td>@if($sub_sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                    
                                                    <td>@if($sub_sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                    <td>Rp.{{ number_format($sub_sub_sub_data->saldo) }}</td>
                                                   
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                        <tr style="font-weight: bold;">
                            <td colspan="5" >Total Pendapatan : Rp.{{ number_format($pendapatan) }}</td>
                        </tr>
                    
                    @elseif($data->sub_akun == 0 && $data->jenis_akun == 5)
                        <tr >
                           
                            <td>{{ $data->nomor_akun }}</td>
                            <td>{{ $data->nama_akun }}</td>
                            
                            <td></td>
                            <td></td>
                            <td></td>
                        
                        </tr>      
                        @foreach($akuns as $sub_data)
                            @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                <tr>
                                    <?php $biaya += (int)$sub_data->saldo; ?>
                                    <td >{{ $sub_data->nomor_akun }}</td>
                                    <td >{{ $sub_data->nama_akun }}</td>
                                    
                                    <td>@if($sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                    
                                    <td>@if($sub_data->tipe_saldo == 1) Debit @elseif($sub_data->tipe_saldo == 2) Kredit @endif</td>
                                    <td>
                                        Rp.{{ number_format($sub_data->saldo) }}
                                    </td>
                                    
                                </tr>
                                @foreach($akuns as $sub_sub_data)
                                    @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                        <tr>
                                            <?php $biaya += (int)$sub_sub_data->saldo; ?>
                                            <td >{{ $sub_sub_data->nomor_akun }}</td>

                                            <td >{{ $sub_sub_data->nama_akun }}</td>
                                            
                                            <td>@if($sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                            
                                            <td>@if($sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                            <td>
                                                <div style="text-align: right; ">Rp. {{ number_format($sub_sub_data->saldo) }}</div>
                                            </td>
                                          
                                        </tr>
                                        @foreach($akuns as $sub_sub_sub_data)
                                            @if($sub_sub_sub_data->sub_akun == $sub_sub_data->id && $sub_sub_sub_data->sub_akun != 0)
                                                <tr>
                                                    
                                                    <td >{{ $sub_sub_sub_data->nomor_akun }}</td>

                                                    <td >{{ $sub_sub_sub_data->nama_akun }}</td>
                                                    
                                                    <td>@if($sub_sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                    
                                                    <td>@if($sub_sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                    <td>Rp.{{ number_format($sub_sub_sub_data->saldo) }}</td>
                                                   
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                        <tr style="font-weight: bold;">
                            <td colspan="5">Total Biaya Operasional : Rp.{{ number_format($biaya) }}</td>
                        </tr>
                        <tr style="font-weight: bold;">
                            <td colspan="5">Total : Rp.{{ number_format($pendapatan-$biaya) }}</td>
                        </tr>
                    @endif
                @endforeach
                
            </tbody>
            
        </table>