@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
        <li class="breadcrumb-item active">Data {{ $title }}</li>
     
    </ol>

    
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">
                    <form action="{{ url($page) }}" method="POST" target="_blank">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label  >Bulan :</label>
                            <select name="bulan" class=" col-md-2">
                                <option value="1">Januari</option>
                                <option value="2">Febuari</option>
                                <option value="3">Maret</option>
                                <option value="4">April</option>
                                <option value="5">Mei</option>
                                <option value="6">Juni</option>
                                <option value="7">Juli</option>
                                <option value="8">Agustus</option>
                                <option value="9">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                            <button class="btn btn-info">Buat Laporan</button>
                        </div>

                        
                    </form>
                      </div>
                <div class="panel-body table-responsive pre-scrollable">
                    
                    <table class="table display">
                        <thead>
                            <tr>
                                <th>Nomor Akun</th>
                                <th>Nama Akun</th>     
                                <th>Tipe Akun</th>
                                <th>Tipe Saldo</th>
                                <th>Saldo</th>
                              
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($akuns as $data)
                                @if($data->sub_akun == 0 && $data->jenis_akun == 4)
                                    <tr >
                                       
                                        <td>{{ $data->nomor_akun }}</td>
                                        <td>{{ $data->nama_akun }}</td>
                                        
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    
                                    </tr>
                                    <?php 
                                        $pendapatan = 0;
                                        $biaya = 0;
                                     ?>
                                    @foreach($akuns as $sub_data)
                                        @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                            <tr>
                                                <?php $pendapatan += (int)$sub_data->saldo; ?>
                                                <td >{{ $sub_data->nomor_akun }}</td>
                                                <td >{{ $sub_data->nama_akun }}</td>
                                                
                                                <td>@if($sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                
                                                <td>@if($sub_data->tipe_saldo == 1) Debit @elseif($sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                <td>
                                                    Rp.{{ number_format($sub_data->saldo) }}
                                                </td>
                                              
                                            </tr>
                                            @foreach($akuns as $sub_sub_data)
                                                @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                                    <tr>
                                                        <?php $pendapatan += (int)$sub_sub_data->saldo; ?>
                                                        <td >{{ $sub_sub_data->nomor_akun }}</td>

                                                        <td >{{ $sub_sub_data->nama_akun }}</td>
                                                        
                                                        <td>@if($sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                        
                                                        <td>@if($sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                        <td>
                                                            <div style="text-align: right;">Rp. {{ number_format($sub_sub_data->saldo) }}</div>
                                                        </td>
                                                      
                                                    </tr>
                                                    @foreach($akuns as $sub_sub_sub_data)
                                                        @if($sub_sub_sub_data->sub_akun == $sub_sub_data->id && $sub_sub_sub_data->sub_akun != 0)
                                                            <tr>
                                                                
                                                                <td >{{ $sub_sub_sub_data->nomor_akun }}</td>

                                                                <td >{{ $sub_sub_sub_data->nama_akun }}</td>
                                                                
                                                                <td>@if($sub_sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                                
                                                                <td>@if($sub_sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                                <td>Rp.{{ number_format($sub_sub_sub_data->saldo) }}</td>
                                                               
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                    <tr style="font-weight: bold;">
                                        <td colspan="5" >Total Pendapatan : Rp.{{ number_format($pendapatan) }}</td>
                                    </tr>
                                
                                @elseif($data->sub_akun == 0 && $data->jenis_akun == 5)
                                    <tr >
                                       
                                        <td>{{ $data->nomor_akun }}</td>
                                        <td>{{ $data->nama_akun }}</td>
                                        
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    
                                    </tr>      
                                    @foreach($akuns as $sub_data)
                                        @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                            <tr>
                                                <?php $biaya += (int)$sub_data->saldo; ?>
                                                <td >{{ $sub_data->nomor_akun }}</td>
                                                <td >{{ $sub_data->nama_akun }}</td>
                                                
                                                <td>@if($sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                
                                                <td>@if($sub_data->tipe_saldo == 1) Debit @elseif($sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                <td>
                                                    Rp.{{ number_format($sub_data->saldo) }}
                                                </td>
                                                
                                            </tr>
                                            @foreach($akuns as $sub_sub_data)
                                                @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                                    <tr>
                                                        <?php $biaya += (int)$sub_sub_data->saldo; ?>
                                                        <td >{{ $sub_sub_data->nomor_akun }}</td>

                                                        <td >{{ $sub_sub_data->nama_akun }}</td>
                                                        
                                                        <td>@if($sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                        
                                                        <td>@if($sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                        <td>
                                                            <div style="text-align: right; ">Rp. {{ number_format($sub_sub_data->saldo) }}</div>
                                                        </td>
                                                      
                                                    </tr>
                                                    @foreach($akuns as $sub_sub_sub_data)
                                                        @if($sub_sub_sub_data->sub_akun == $sub_sub_data->id && $sub_sub_sub_data->sub_akun != 0)
                                                            <tr>
                                                                
                                                                <td >{{ $sub_sub_sub_data->nomor_akun }}</td>

                                                                <td >{{ $sub_sub_sub_data->nama_akun }}</td>
                                                                
                                                                <td>@if($sub_sub_sub_data->tipe_akun == 1) Cash/Bank @elseif($sub_sub_sub_data->tipe_akun == 2) Non Cash/Bank @endif</td>
                                                                
                                                                <td>@if($sub_sub_sub_data->tipe_saldo == 1) Debit @elseif($sub_sub_sub_data->tipe_saldo == 2) Kredit @endif</td>
                                                                <td>Rp.{{ number_format($sub_sub_sub_data->saldo) }}</td>
                                                               
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                    <tr style="font-weight: bold;">
                                        <td colspan="5">Total Biaya Operasional : Rp.{{ number_format($biaya) }}</td>
                                    </tr>
                                    <tr style="font-weight: bold;">
                                        <td colspan="5">Total : Rp.{{ number_format($pendapatan-$biaya) }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        
                    </table>


                    
                   
                </div>
            </div>
        </div>
    </div>
</div>


@endsection