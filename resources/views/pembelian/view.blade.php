@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Lihat Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-title"> Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$data->id) }}" method="POST">
                        {{ csrf_field() }}
                        
                        <input disabled type="hidden" name="_method" value="PUT">
                        
                        <div class="form-group">
                            <label for="nomor" class="form-label" >Nomor</label>
                            <input disabled type="text" name="nomor" class="form-control" value="{{ $data->nomor }}">
                
                        </div> 


                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input disabled type="text" id="date-picker" name="tanggal" class="form-control" value="{{ $data->tanggal }}" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                                                
                        
                        
                     

                        <div class="form-group">
                            <label for="user_id" class="form-label" >Di Setujui Oleh</label>
                            <select  disabled name="user_id" class="form-control">
                                @foreach($user as $jj)
                                    
                                        <option @if($data->user_id == $jj->id) selected @endif value="{{ $jj->id }}">{{ $jj->name }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>


                        <div class="form-group">
                            <label for="user_id" class="form-label" >Pilih Permintaan</label>
                            <select disabled disabled="" name="user_id" class="form-control">
                                @foreach($permintaan as $jj)
                                    
                                        <option @if($data->permintaan_id == $jj->id) selected @endif value="{{ $jj->id }}">{{ $jj->nomor }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>
                        
                       
                        
                       
                       

                       
                        <div style="clear: both;"></div>
               
                        <br>
                        <br>
                        <table id="example0" class="table display">
                            <thead>
                                <?php $no = 1; ?>
                                <tr>
                                    <th>No</th>
                                    <th>Urain</th>
                                    <th>Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Unit</th>
                                    <th>Jumlah</th>
                                    <th>Harga (Rp.)</th>
                                    <th>Supplier</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total = 0;$ttl_harga = 0;?>
                                @foreach($pembelian as $dd)
                                    <tr>
                                        <input type="hidden" name="id_detail[]" value="{{ $dd->id }}">
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $dd->uraian }}</td>
                                        <td>
                                            {{ $dd->kode_barang }}
                                            <input type="hidden" name="kode_barang[]" value="{{ $dd->kode_barang }}">
                                        </td>
                                        <td>{{ TrueHelper::nama_barang($dd->kode_barang) }}</td>
                                        <td>{{ TrueHelper::nama_unit($dd->kode_unit) }}</td>
                                        <td>{{ $dd->jumlah }}</td>
                                        <td>{{ $dd->harga }}</td>
                                        <?php $ttl_harga+=$dd->harga;$total+=$dd->jumlah; ?>
                                        <td>
                                            <select  name="supplier_id[]" disabled="">
                                                <option>Pilih Supplier</option>
                                                @foreach($supplier as $jj)
                                                    
                                                        <option @if($dd->supplier_id == $jj->id) selected @endif value="{{ $jj->id }}">{{ $jj->nama_suplier }}</option>
                                                    
                                                @endforeach
                                            </select>
                                        </td>
                                        
                                        
                                    </tr>
                                    
                                @endforeach
                                <tr>
                                    <td colspan="6" style="text-align: right;">Total : {{ $total }}</td>
                                    <td colspan="2">Rp.{{ number_format($ttl_harga) }}</td>
                                </tr>
                                
                            </tbody>
                            
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>


@endsection