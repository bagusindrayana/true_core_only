@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
        <li class="breadcrumb-item active">Data {{ $title }}</li>
     
    </ol>

    
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">
                    <form action="{{ url($page) }}" method="POST" target="_blank">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label for="nomor_akun" class=" col-md-2" >Dari Tanggal :</label>
                            <input type="date" class=" col-md-2" name="dari_tanggal">
                            <label for="nomor_akun" class=" col-md-2" >Sampai Tanggal :</label>
                            <input type="date" class=" col-md-2" name="sampai_tanggal"> 
                            <button class="btn btn-info">Buat Laporan</button>
                        </div>

                        
                    </form>
                      </div>
                <div class="panel-body table-responsive">
                    
                    <?php 
                    $akun = \App\Akun::where('nama_akun','KAS')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <h2>Kas</h2>
                    <table  class="table display" id="sum_table1">
                        <thead>
                            
                            <tr class="titlerow">
                                <th  >No</th>
                                <th  >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                                
                                <th  >Debit</th>
                                <th  >Kredit</th>
                                <th  >Saldo</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php $no = 1; ?>
                             @foreach($jurnalumum as $dt)
                                @foreach($dt->detail as $data)
                                    @if($data->akun->id == $akun->id)
                                        <?php $saldo += $data->debit; ?>
                                        <?php $saldo += $data->kredit; ?>

                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ date('d-M-Y',strtotime($dt->tanggal)) }}</td>
                                            <td>{{$dt->no_jurnal}}</td>
                                            <td>{{$dt->uraian}}</td>
                                            <td class="sum" >{{ $data->debit }}</td>
                                            <td class="sum">{{ $data->kredit }}</td>
                                            <td>{{ $saldo }}</td>
                                        </tr>
                                    @endif
                                @endforeach

                             
                            @endforeach
                            

                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo -= $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->untuk}}</td>
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)
                                @if($data->akun1->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->uraian}}</td>
                                        
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        <tfoot >
                            <tr class="summary">
                              <td colspan="3"></td>
                              <td >Total:</td>
                              <td class="first">?</td>
                              <td class="second">?</td>
                              <td class="third">?</td>
                            </tr>
                        </tfoot>
                    </table>
                    <br>
                    <br>
                    <br>
                    <?php 
                    $akun = \App\Akun::where('nama_akun','BANK MANDIRI PT.TRUE')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <h2>Bank</h2>
                    <table  class="table display" id="sum_table2">
                        <thead>
                            
                            <tr>
                                <th  >No</th>
                                <th  >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                                
                                <th  >Debit</th>
                                <th  >Kredit</th>
                                <th  >Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            
                             @foreach($tagihan as $data)
                                    @if($data->ppn == 1)
                                        <?php $ppn = $data->saldo*(10/100); ?>
                                       
                                    @endif
                                    @if($data->pph == 1)
                                        <?php $pph = $data->saldo*(2/100); ?>
                                        
                                    @endif
                                    <?php $saldo += $data->saldo+$ppn-$pph; ?>
                                    
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->no}}</td>
                                        <td>{{ $data->uraian }}</td>
                                        <td class="sum">{{ $data->saldo+$ppn-$pph }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                
                            @endforeach

                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo -= $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->untuk}}</td>
                                        
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)
                                @if($data->akun1->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->uraian}}</td>
                                    
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach

                            @foreach($jurnalumum as $dt)
                                @foreach($dt->detail as $data)
                                    @if($data->akun->id == $akun->id)
                                        <?php $saldo -= $data->debit; ?>
                                        <?php $saldo -= $data->kredit; ?>

                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ date('d-M-Y',strtotime($dt->tanggal)) }}</td>
                                            <td>{{$dt->no_jurnal}}</td>
                                            <td>{{$dt->uraian}}</td>
                                         
                                            <td class="sum">{{ $data->debit }}</td>
                                            <td class="sum">{{ $data->kredit }}</td>
                                            <td>{{ $saldo }}</td>
                                        </tr>
                                    @endif
                                @endforeach

                             
                            @endforeach
                            
                        </tbody>
                        <tfoot >
                            <tr class="summary">
                              <td colspan="3"></td>
                              <td >Total:</td>
                              <td class="first">?</td>
                              <td class="second">?</td>
                              <td class="third">?</td>
                            </tr>
                        </tfoot>
                    </table>
                    <br>
                    <br>
                    <br>
                    <?php 
                    $akun = \App\Akun::where('nama_akun','PIUTANG USAHA')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <h2>Piutang Usaha</h2>
                    <table  class="table display" id="sum_table3">
                        <thead>
                                                        <tr>
                                <th  >No</th>
                                <th  >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                               
                                <th  >Debit</th>
                                <th  >Kredit</th>
                                <th  >Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                           
                            @foreach($tagihan as $data)
                                    @if($data->ppn == 1)
                                        <?php $ppn = $data->saldo*(10/100); ?>
                                       
                                    @endif
                                    @if($data->pph == 1)
                                        <?php $pph = $data->saldo*(2/100); ?>
                                        
                                    @endif
                                    <?php $saldo += $data->saldo+$ppn-$pph; ?>
                                    
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->no}}</td>
                                        <td>Piutang Penjualan dengan No.{{ $data->no }}</td>
                                       
                                        <td class="sum">{{ $data->saldo+$ppn-$pph }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                
                            @endforeach

                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo -= $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->untuk}}</td>
                                       
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)

                                @if($data->akun1->id == $akun->id)
                                    
                                    <?php $saldo += $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->uraian}}</td>
                                        
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        <tfoot >
                            <tr class="summary">
                              <td colspan="3"></td>
                              <td >Total:</td>
                              <td class="first">?</td>
                              <td class="second">?</td>
                              <td class="third">?</td>
                            </tr>
                        </tfoot>
                    </table>
                    <br>
                    <br>
                    <br>
                    <?php 
                    $akun = \App\Akun::where('nama_akun','PAJAK DIBAYAR DIMUKA')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <h2>Pajak Dibayar Dimuka</h2>
                    <table  class="table display" id="sum_table4">
                        <thead>
                          
                            <tr>
                                <th  >No</th>
                                <th  >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                                <th  >Debit</th>
                                <th  >Kredit</th>
                                <th  >Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            
                            @foreach($tagihan as $data)
                                    @if($data->ppn == 1)
                                        <?php $ppn = $data->saldo*(10/100); ?>
                                       
                                    @endif
                                    @if($data->pph == 1)
                                        <?php $pph = $data->saldo*(2/100); ?>
                                        
                                    @endif
                                    <?php $saldo += $pph; ?>
                                    
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->no}}</td>
                                        <td>PPh 23 Invoice No.{{ $data->no }}</td>
                                       
                                        <td class="sum">{{ $pph }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                
                            @endforeach

                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo -= $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->untuk}}</td>
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)
                                @if($data->akun1->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->uraian}}</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        <tfoot >
                            <tr class="summary">
                              <td colspan="3"></td>
                              <td >Total:</td>
                              <td class="first">?</td>
                              <td class="second">?</td>
                              <td class="third">?</td>
                            </tr>
                        </tfoot>
                    </table>
                    <br>
                    <br>
                    <br>
                    <?php 
                    $akun = \App\Akun::where('nama_akun','UTANG PAJAK')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <h2>Utang Pajak</h2>
                    <table  class="table display" id="sum_table5">
                        <thead>
                          
                            <tr>
                                <th  >No</th>
                                <th  >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                              
                                <th  >Debit</th>
                                <th  >Kredit</th>
                                <th  >Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                           
                            @foreach($tagihan as $data)
                                    @if($data->ppn == 1)
                                        <?php $ppn = $data->saldo*(10/100); ?>
                                       
                                    @endif
                                    @if($data->pph == 1)
                                        <?php $pph = $data->saldo*(2/100); ?>
                                        
                                    @endif
                                    <?php $saldo -= $data->saldo; ?>
                                    
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->no}}</td>
                                        <td>PPN No.{{ $data->no }}</td>
                                       
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td><?php if($saldo < 0){ echo "(".str_replace('-', '', $saldo).")"; } else {echo $saldo;} ?></td>
                                    </tr>
                                
                            @endforeach
                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo -= $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->untuk}}</td>
                                 
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)
                                @if($data->akun1->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->uraian}}</td>
                                        
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        <tfoot >
                            <tr class="summary">
                              <td colspan="3"></td>
                              <td >Total:</td>
                              <td class="first">?</td>
                              <td class="second">?</td>
                              <td class="third">?</td>
                            </tr>
                        </tfoot>
                    </table>
                    <br>
                    <br>
                    <br>
                    <?php 
                    $akun = \App\Akun::where('nama_akun','PENDAPATAN USAHA')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <h2>Pendapatan Usaha</h2>
                    <table  class="table display" id="sum_table6">
                        <thead>
                           
                            <tr>
                                <th  >No</th>
                                <th  >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                                <th  >Debit</th>
                                <th  >Kredit</th>
                                <th  >Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                           
                            @foreach($tagihan as $dt)
                                    @foreach($dt->detail as $data)
                                           
                                            <?php $saldo -= $data->jumlah; ?>
                                            
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ date('d-M-Y',strtotime($dt->tanggal)) }}</td>
                                                <td>{{ $dt->no }}</td>
                                                <td>Penjualan Jasa {{ $dt->costomer->nama_costomer }}</td>
                                                <td class="sum">0</td>
                                                <td class="sum">{{ $data->jumlah }}</td>
                                                <td><?php if($saldo < 0){ echo "(".str_replace('-', '', $saldo).")"; } else {echo $saldo;} ?></td>
                                            </tr>
                                        
                                    @endforeach
                                
                            @endforeach

                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo -= $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->untuk}}</td>
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)
                                @if($data->akun1->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->uraian}}</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        <tfoot >
                            <tr class="summary">
                              <td colspan="3"></td>
                              <td >Total:</td>
                              <td class="first">?</td>
                              <td class="second">?</td>
                              <td class="third">?</td>
                            </tr>
                        </tfoot>
                    </table>

                    <br>
                    <br>
                    <br>
                    <?php 
                    $akun = \App\Akun::where('nama_akun','BIAYA BBM KENDARAAN OPERASIONAL')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <h2>Biaya BBM Kendaraan Operasional</h2>
                    <table  class="table display" id="sum_table7">
                        <thead>
                             
                            <tr>
                                <th  >No</th>
                                <th  >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                                
                                <th  >Debit</th>
                                <th  >Kredit</th>
                                <th  >Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                           

                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{ $data->nomor }}</td>
                                        <td>{{ $data->untuk }}</td>
                                        
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo -= $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->uraian}}</td>
                                        <td class="sum">0</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        <tfoot >
                            <tr class="summary">
                              <td colspan="3"></td>
                              <td >Total:</td>
                              <td class="first">?</td>
                              <td class="second">?</td>
                              <td class="third">?</td>
                            </tr>
                        </tfoot>
                    </table>


                    <br>
                    <br>
                    <br>
                    <?php 
                    $akun = \App\Akun::where('nama_akun','BIAYA PEMAKAIAN OLI ALAT BERAT')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <h2>Biaya Pemakaiaan Oli Alat Berat</h2>
                    <table  class="table display" id="sum_table8">
                        <thead>
                            
                            <tr>
                                <th  >No</th>
                                <th  >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                                <th  >Debit</th>
                                <th  >Kredit</th>
                                <th  >Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                           

                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->untuk}}</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)
                                @if($data->akun1->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ date('d-M-Y',strtotime($data->tanggal)) }}</td>
                                        <td>{{$data->nomor}}</td>
                                        <td>{{$data->uraian}}</td>
                                        <td class="sum">{{ $data->saldo }}</td>
                                        <td class="sum">0</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        <tfoot >
                            <tr class="summary">
                              <td colspan="3"></td>
                              <td >Total:</td>
                              <td class="first">?</td>
                              <td class="second">?</td>
                              <td class="third">?</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script> 
<script type='text/javascript' src="{{ url('jquery.sumtr.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    var id = 1;
    $('.table').each(function(col) {
        
        $('#sum_table'+id+' th').each(function(col) {
        
        if($(this).html() != "Tanggal"){
            return;
        }
          if ($(this).is('.asc')) {
            $(this).removeClass('asc');
            $(this).addClass('desc selected');
            sortOrder = -1;
          }
          else {
            $(this).addClass('asc selected');
            $(this).removeClass('desc');
            sortOrder = 1;
          }
          $(this).siblings().removeClass('asc selected');
          $(this).siblings().removeClass('desc selected');
          var arrData = $('#sum_table'+id+' ').find('tbody >tr:has(td)').get();
          arrData.sort(function(a, b) {
            var val1 = $(a).children('td').eq(col).text().toUpperCase();
            var val2 = $(b).children('td').eq(col).text().toUpperCase();
            if($.isNumeric(val1) && $.isNumeric(val2))
            return sortOrder == 1 ? val1-val2 : val2-val1;
            else
               return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
          });
          $.each(arrData, function(index, row) {
            $('#sum_table'+id+' tbody').append(row);
          });
        
      });
        $('#sum_table'+id).sumtr();
        $('#sum_table'+id+' .summary').sumtr('.first', '.second');
        var str = parseInt($('#sum_table'+id+' .first').html()) - parseInt($('#sum_table'+id+' .second').html());
        str = str.toString();
        if(str < 0){
            str = "("+str.replace(/-/g,"")+")";
        }
        $('#sum_table'+id+' .third').html(str);
        id++;
        

    });
    

        


});


</script>
@endsection





