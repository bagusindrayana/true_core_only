@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Supplier</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data Supplier</a></li>
        <li class="breadcrumb-item"><a href="#">Edit Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Edit Data Data Supplier
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$supplier->id) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="nama_supplier" class="form-label">Nama Supplier</label>
                            <input required type="text" class="form-control" id="nama_supplier" name="nama_supplier" value="{{ $supplier->nama_supplier }}">
                        </div>

                        <div class="form-group">
                            <label for="alamat" class="form-label">Alamat</label>
                            <input required type="text" class="form-control" id="alamat" name="alamat" value="{{ $supplier->alamat }}">
                        </div>

                        <div class="form-group">
                            <label for="no_telepon" class="form-label">No Telepon</label>
                            <input required type="text" class="form-control" id="no_telepon" name="no_telepon" value="{{ $supplier->no_telepon }}">
                        </div>
                       

                       
                        
                        <button type="submit" class="btn btn-default">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection