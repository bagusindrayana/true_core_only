@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Edit Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Edit Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$data->id) }}" method="POST">
                        {{ csrf_field() }}
                       
                        <input type="hidden" name="_method" value="PUT">
                        
                        <div class="form-group">
                            <label for="nomor" class="form-label" >Nomor</label>
                            <input type="text" name="nomor" class="form-control" value="{{ $data->nomor }}">
                
                        </div> 


                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" id="date-picker" name="tanggal" class="form-control" value="{{ $data->tanggal }}"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                                                
                        
                        
                     

                        <div class="form-group">
                            <label for="diminta_id" class="form-label" >Di Minta Oleh</label>
                            <select name="diminta_id" class="form-control">
                                @foreach($user as $dx)
                                    
                                        <option  value="{{ $dx->id }}">{{ $dx->name }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>
                        <div style="clear: both;"></div>
                        <button type="submit" class="btn btn-default" name="tombol" value="ubah">Simpan</button>
                        
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Detail {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$data->id) }}" method="POST">
                        {{ csrf_field() }}
                       
                        <input type="hidden" name="_method" value="PUT">
                        
                        <div class="form-group">
                            <label for="uraian" class="form-label" >Uraian</label>
                            <input type="text" name="uraian" class="form-control">
                        </div>
                        
                        <div class="form-group">
                            <label for="kode_unit" class="form-label" >Unit</label>
                            <select name="kode_unit" class="form-control">
                                <option>Pilih Unit</option>
                                @foreach($units as $dx)
                                    <option  value="{{ $dx->kode_unit }}">{{ $dx->nama_unit }}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="kode_barang" class="form-label" >Barang</label>
                            <select name="kode_barang" class="form-control">
                                <option>Pilih Barang</option>
                                @foreach($barangs as $dx)
                                    
                                        <option  value="{{ $dx->kode_barang }}">{{ $dx->kode_barang }} - {{ $dx->nama_barang }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>

                        <div class="form-group">
                            <label for="jumlah" class="form-label" >Jumlah</label>
                            <input type="number" name="jumlah" class="form-control">
                        </div> 
                        <div style="clear: both;"></div>
                         <button type="submit" class="btn btn-default" name="tombol" value="tambah_detail">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">Data Detail {{ $title }}  </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display table-bordered">
                        <thead>
                            <?php $no = 1; ?>
                            <tr>
                                <th width="2px" class="text-center">No</th>
                                <th class="text-center">Kode Barang</th>
                                <th class="text-center">Nama Barang</th>
                                <th class="text-center">Unit</th>
                                <th class="text-center">Jumlah</th>
                                <th class="text-center">Urain</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            @foreach($data->detail as $dd)
                            <?php $total += $dd->jumlah; ?>
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">{{ $dd->barang->kode_barang }}</td>
                                    <td>{{ $dd->barang->nama_barang }}</td>
                                    <td>{{ $dd->unit->nama_unit }}</td>
                                    <td class="text-center">{{ $dd->jumlah }}</td>
                                    <td>{{ $dd->uraian }}</td>
                                    <td class="text-center">
                                        <a href="{{url('detail_permintaan/delete/'.$dd->id)}}">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                           <tr>
                               <td colspan="8" style="text-align: right;font-weight: bold;padding-right: 35%;">Total : {{ $total }}</td>
                           </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection