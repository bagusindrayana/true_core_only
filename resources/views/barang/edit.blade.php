@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Barang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data Barang</a></li>
        <li class="breadcrumb-item"><a href="#">Edit Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Edit Data Data Barang
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$barang->kode_barang) }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label for="kode_barang" class="form-label">Kode Barang</label>
                            <input required type="text" class="form-control" id="kode_barang" name="kode_barang" value="{{ $barang->kode_barang }}">
                        </div>

                        <div class="form-group">
                            <label for="nama_barang" class="form-label">Nama Barang</label>
                            <input required type="text" class="form-control" id="nama_barang" name="nama_barang" value="{{ $barang->nama_barang }}">
                        </div>

                        <div class="form-group">
                            <label for="merek_barang" class="form-label">Merek Barang</label>
                            <input required type="text" class="form-control" id="merek_barang" name="merek_barang" value="{{ $barang->merek_barang }}">
                        </div>

                        <div class="form-group">
                            <label for="gambar" class="form-label">Gambar Barang</label>
                            <br>
                            <img src="{{ asset('images/barang/'.$barang->gambar_barang)  }}" style="max-width: 60%;">
                            <br>
                            <input type="file" class="form-control" id="gambar" name="gambar">
                            <p>type gambar jpeg,jpg,png dengan size max 5mb</p>
                        </div>
                       

                       
                        
                        <button type="submit" class="btn btn-default">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection