@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active">Data {{ $title }}</li>
        <li style="float: right;"><a href="{{ url($page.'/create') }}" class="btn btn-default">Tambah Data</a> </li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"></div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Level</th>
                                <th>Daftar Akun</th>
                                <th>Costomer</th>
                                <th>Supplier</th>
                                <th>Barang</th>
                                <th>Pemasukan</th>
                                <th>Pengeluaran</th>
                                <th>Transfer</th>
                                <th>Permintaan</th>
                                <th>Pembelian</th>
                                <th>Tagihan</th>
                                <th>Penerimaan</th>
                                <th>Jurnal Umum</th>
                                <th>Transaksi Kas</th>
                                <th>Buku Besar</th>
                                <th>Neraca Saldo</th>
                                <th>Saldo Kas</th>
                                <th>Jatuh Tempo</th>
                                <th>Laba Rugi</th>
                                <th>Profil</th>
                                <th>Level Pengguna</th>
                                <th>Pengguna</th>
                                
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($levelusers as $data)
                                <tr>
                                    
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->nama_level }}</td>
                                    <td>{{ $data->daftar_akun }}</td>
                                    <td>{{ $data->costomer }}</td>
                                    <td>{{ $data->supplier }}</td>
                                    <td>{{ $data->barang }}</td>
                                    <td>{{ $data->pemasukan }}</td>
                                    <td>{{ $data->pengeluaran }}</td>
                                    <td>{{ $data->transfer }}</td>
                                    <td>{{ $data->permintaan }}</td>
                                    <td>{{ $data->pembelian }}</td>
                                    <td>{{ $data->transaksi_kas }}</td>
                                    <td>{{ $data->buku_besar }}</td>
                                    <td>{{ $data->tagihan }}</td>
                                    <td>{{ $data->permintaan }}</td>
                                    <td>{{ $data->jurnal_umum }}</td>
                                    <td>{{ $data->neraca_saldo }}</td>
                                    <td>{{ $data->saldo_kas }}</td>
                                    <td>{{ $data->jatuh_tempo }}</td>
                                    <td>{{ $data->laba_rugi }}</td>
                                    <td>{{ $data->profil }}</td>
                                    <td>{{ $data->level_pengguna }}</td>
                                    <td>{{ $data->pengguna }}</td>
                                    
                                    <td class="aksi">
                                        <a href="{{ url($page.'/'. $data->id.'/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete<?php echo $data->id;  ?>').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url($page.'/'.$data->id) }}" id="delete<?php echo $data->id;  ?>" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection