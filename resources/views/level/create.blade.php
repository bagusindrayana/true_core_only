@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST">
                        {{ csrf_field() }}
                        

                        

                      

                      

                        <div class="form-group">
                            <label for="nama_level" class="form-label">Nama Level</label>
                            <input required type="text" class="form-control" id="nama_level" name="nama_level">
                        </div>
                       
                        <div class="form-group">
                            <h4>Pilih Hak Akses Menu</h4>
                
                        </div>
                   

                       
                            <div  style="float: left;width: 30%;">
                                <label  class="form-label" >Jenis Akun</label> <input type="checkbox" value="1" name="daftar_akun">
                                <br>
                                <label  class="form-label" >Data Kas</label> <input type="checkbox" value="1" name="data_kas">
                                <br>
                                <label  class="form-label" >Supplier</label> <input type="checkbox" value="1" name="supplier">
                                <br>
                                <label  class="form-label" >Barang</label> <input type="checkbox" value="1" name="barang">
                                <br>
                            </div>
                            <div  style="float: left;width: 30%;">
                               
                                
                                <label  class="form-label" >Pemasukan</label> <input type="checkbox" value="1" name="pemasukan">
                                <br>
                                <label  class="form-label" >Pengeluaran</label> <input type="checkbox" value="1" name="pengeluaran">
                                <br>
                                <label  class="form-label" >Transfer</label> <input type="checkbox" value="1" name="transfer">
                                <br>
                                <label  class="form-label" >Permintaan</label> <input type="checkbox" value="1" name="permintaan">
                                <br>
                            </div>
                            <div  style="float: left;width: 30%;">
                               
                                
                                
                                <label  class="form-label" >Pembelian</label> <input type="checkbox" value="1" name="pembelian">
                                <br>
                                <label  class="form-label" >Pemakaian</label> <input type="checkbox" value="1" name="pemakaian">
                                <br>
                                <label  class="form-label" >Transaksi Kas</label> <input type="checkbox" value="1" name="transaksi_kas">
                                <br>
                                <label  class="form-label" >Buku Besar</label> <input type="checkbox" value="1" name="buku_besar">
                                <br>
                            </div>

                            <div  style="float: left;width: 30%;">
                               
                                
                                
                                <label  class="form-label" >Neraca Saldo</label> <input type="checkbox" value="1" name="neraca_saldo">
                                <br>
                                <label  class="form-label" >Saldo Kas</label> <input type="checkbox" value="1" name="saldo_kas">
                                <br>
                                <label  class="form-label" >Jatuh Tempo</label> <input type="checkbox" value="1" name="jatuh_tempo">
                                <br>
                                <label  class="form-label" >Laba Rugi</label> <input type="checkbox" value="1" name="laba_rugi">
                                <br>
                            </div>

                            <div  style="float: left;width: 30%;">
                               
                                
                                
                                <label  class="form-label" >Profil</label> <input type="checkbox" value="1" name="profil">
                                <br>
                                <label  class="form-label" >Level Pengguna</label> <input type="checkbox" value="1" name="level_pengguna">
                                <br>
                                <label  class="form-label" >Pengguna</label> <input type="checkbox" value="1" name="pengguna">
                                <br>
                                <label  class="form-label" >Tagihan</label> <input type="checkbox" value="1" name="tagihan">
                                <br>
                            </div>

                            <div  style="float: left;width: 30%;">
                               
                                
                                
                                <label  class="form-label" >Penerimaan</label> <input type="checkbox" value="1" name="penerimaan">
                                <br>
                                <label  class="form-label" >Jurnal Umum</label> <input type="checkbox" value="1" name="jurnal_umum">
                                <br>
                                <label  class="form-label" >Customer</label> <input type="checkbox" value="1" name="costomer">
                                <br>
                            </div>
                                
                            
                            <div style="clear: both;margin-bottom: 50px;"></div>
                       

                       
                        
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>


@endsection