@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST">
                        {{ csrf_field() }}
                        

                        

                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" id="date-picker" name="tanggal" class="form-control" value="<?php echo date('Y-m-d') ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                      

                        <div class="form-group">
                            <label for="uraian" class="form-label">Uraian</label>
                            <textarea required type="text" class="form-control" id="uraian" name="uraian"></textarea>
                        </div>
                       

                       <div class="form-group">
                            <label for="kas1" class="form-label" >Dari Kas</label>
                            <input type="text" name="kas1" readonly="" id="id_kas1" required="" class="form-control">
                            <button type="button"  data-toggle="modal" data-target="#kas" onclick="pilih_target('#id_kas1')">Pilih Kas</button> 
                
                        </div>

                        <div class="form-group">
                            <label for="kas2" class="form-label" >Ke Kas</label>
                            <input type="text" name="kas2" readonly="" id="id_kas2" required="" class="form-control">
                            <button type="button"  data-toggle="modal" data-target="#kas" onclick="pilih_target('#id_kas2')">Pilih Kas</button> 
                
                        </div>

                        <div class="form-group">
                            <label for="jumlah" class="form-label" >Jumlah</label>
                            <input type="number" name="jumlah" required="" class="form-control">
                            
                
                        </div>
                       

                       
                        
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="modal fade" id="kas"  >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Data Kas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Kas</th>
                                
                               
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kas as $data)
                                <tr  id="{{ $data->id}}-list">
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->nama_kas }}</td>
                                   
                                    
                                    <td>
                                        <a href="#" class="btn btn-info" onclick="pilih_data_2('{{ $data->id }}','#id_kas')" data-dismiss="modal">Pilih</a>
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>


@endsection

