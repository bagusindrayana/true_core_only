@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST">
                        {{ csrf_field() }}
                        

                        

                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input required type="text" id="date-picker" name="tanggal" class="form-control" placeholder="Tanggal" value="<?php echo date('m/d/Y') ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <div class="form-group">
                            <label for="nomor" class="form-label" >Nomor</label>
                            <input type="text" name="nomor" class="form-control">
                
                        </div> 

                  
                        <div class="form-group">
                            <label for="id_costomer" class="form-label" >Costomer</label>
                            <select name="id_costomer" class="form-control">
                                <option>Pilih Costomer</option>
                                @foreach($costomers as $data)
                                    
                                        <option  value="{{ $data->id }}">{{ $data->nama_costomer }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div> 
                       
               

                       <div class="form-group">
                            <label for="akun1" class="form-label" >Ke akun (Debit)</label>
                            
                            <select name="id_akun1" class="form-control">
                                <option>Pilih Akun</option>
                                @foreach($akuns as $data)
                                    @if($data->tipe_akun == 1 && $data->kategori_akun == 2)
                                        <option  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                    @endif
                                @endforeach
                            </select>
                             
                
                        </div>

                        

                        <div class="form-group">
                            <label for="akun2" class="form-label" >Dari akun (Kredit)</label>
                            <select name="id_akun2" class="form-control">
                                <option>Pilih Akun</option>
                                
                                @foreach($akuns as $data)
                                    @if($data->tipe_saldo == 2 && $data->kategori_akun == 2 && $data->tipe_akun != 1)
                                        <option  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                    @endif
                                @endforeach
                            </select>
                
                        </div>

                        <div class="form-group">
                            <label for="uraian_pemasukan" class="form-label" >Uraian</label>
                            <input type="text" name="uraian_pemasukan" class="form-control">
                
                        </div> 

                        <div class="form-group">
                            <label for="untuk" class="form-label" ></label>
                            <h5>Detail Pemasukan</h5>
                
                        </div>
                            
                        <div class="form-group">
                            <label for="kode_unit" class="form-label" >Unit</label>
                            <select name="kode_unit" class="form-control">
                                <option>Pilih Unit</option>
                                @foreach($units as $data)
                                    
                                        <option  value="{{ $data->kode_unit }}">{{ $data->nama_unit }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>

                        <div class="form-group">
                            <label for="uraian" class="form-label">Uraian</label>
                            <input required  required type="text" class="form-control" id="uraian" name="uraian">
                        </div>

                        <div class="form-group">
                            <label for="qty" class="form-label" >QTY</label>
                            <input required type="number" name="qty" required="" class="form-control">
                            
                
                        </div>

                        <div class="form-group">
                            <label for="harga_satuan" class="form-label" >Harga Satuan (Rp.)</label>
                            <input required type="number" name="harga_satuan" required="" class="form-control">
                            
                
                        </div>
                        <div style="clear: both;"></div>
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    

</div>




@endsection