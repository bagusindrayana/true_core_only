@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title">Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="">
                        
                        
                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input readonly type="text" id="date-picker" name="tanggal" class="form-control" value="{{ $pemasukan->tanggal }}" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                      

                        <div class="form-group">
                            <label for="untuk" class="form-label" >Costomer</label>
                            <input readonly type="text" name="untuk" class="form-control" value="{{ $pemasukan->costomer->nama_costomer }}">
                
                        </div> 
                  
                       <div class="form-group">
                            <label for="akun1" class="form-label" >Ke akun (Kredit)</label>
                            
                            <input type="text" name="" readonly="" value="{{ $pemasukan->akun1->nama_akun }}" class="form-control">
                             
                
                        </div>

                        <div class="form-group">
                            <label for="akun2" class="form-label" >Dari akun (Debit)</label>
                            <input type="text" name="" readonly="" value="{{ $pemasukan->akun2->nama_akun }}" class="form-control">
                
                        </div>
                        <div style="clear: both;"></div>
                       

                        
                        
                    </form>
                </div>
            </div>
        </div>
       
    </div>
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">Data Detail {{ $title }}  </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Unit</th>
                               
                                <th>Urain</th>
                                <th>QTY</th>
                                <th>Harga Satuan</th>
                              
                                <th>Jumlah</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no =1;$total = 0; ?>
                            @foreach($pemasukan->detail as $data)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $data->unit->nama_unit }}</td>
                                    <td>{{ $data->uraian }}</td>
                         
                                    <td>{{ $data->qty }}</td>
                                    <td>Rp.{{ number_format($data->harga_satuan,2) }}</td>
                                    <td>Rp.{{ number_format($data->jumlah,2) }}</td>
                                    
                                    
                                    
                                </tr>
                                <?php $total += $data->jumlah; ?>
                            @endforeach
                            <tr>
                                <td colspan="4"></td>
                                <td><strong>Total : </strong></td>
                                <td><strong>Rp.{{ number_format($total,2) }}</strong></td>
                                
                                    
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection