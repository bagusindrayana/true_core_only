@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Profil</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data Profil</a></li>
        <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
        
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Profil
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$profil->id) }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label for="nama_profil" class="form-label">Nama Profil</label>
                            <input required type="text" class="form-control" id="nama_profil" name="nama_profil" value="{{$profil->nama}}">
                        </div>


                        <div class="form-group">
                            <label for="logo" class="form-label">Gambar Logo Profil</label>
                            
                            @if($profil->logo != '') 
                                <img src="{{ asset('images/profil/'.$profil->logo)  }}" style="max-width: 60%;">
                            @endif
                           

                            
                        </div>

                        <div class="form-group">
                            <label class="form-label"></label>
                            <input class="form-control" type="file"  id="logo" name="logo" >
                            <span >tipe gambar logo jpeg,jpg,png dengan size max 5mb</span>

                            
                        </div>
                     
                        <div class="form-group">
                            <label for="pakai_logo" class="form-label">Pakai Gambar Logo</label>

                            <input type="checkbox" @if($profil->pakai_logo == 1) checked="checked" @endif name="pakai_logo">
                            
                        </div>
                        <br>
                        

                        <div class="form-group">
                            <label for="alamat" class="form-label" style="float: left;">Alamat</label>
                            <textarea  class="form-control" name="alamat">{{$profil->alamat}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="direktur" class="form-label">Direktur</label>
                            <input required type="text" class="form-control" id="direktur" name="direktur" value="{{$profil->direktur}}">
                        </div>

                        <div class="form-group">
                            <label for="nama_bank" class="form-label">Nama Bank</label>
                            <input required type="text" class="form-control" id="nama_bank" name="nama_bank" value="{{$profil->nama_bank}}">
                        </div>

                        <div class="form-group">
                            <label for="rekening_bank" class="form-label">Rekening Bank</label>
                            <input required type="text" class="form-control" id="rekening_bank" name="rekening_bank" value="{{$profil->rekening_bank}}">
                        </div>

                       

                 
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <button type="submit" class="btn btn-default">Simpan</button>
                        <br>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection