@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Pajak</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data Pajak</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data Pajak
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label  class="form-label">Jenis Transaksi</label>
                            <select class="form-control" name="jenis_transaksi">
                                <option value="1">Tagihan</option>
                                <option value="2">Pemasukan</option>
                                <option value="3">Pengeluaran</option>

                            </select>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Akun Debit</label>
                            <select name="akun_debit_id" class="form-control">
                                <option>Pilih Akun</option>
                                @foreach($akuns as $data)
                                    @if($data->tipe_akun == 1 && $data->kategori_akun == 2)
                                        <option  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                    @endif
                                @endforeach
                            </select>
                            
                        </div>

                        <div class="form-group">
                            <label class="form-label">Akun Kredit</label>
                            <select name="akun_kredit_id" class="form-control">
                                <option>Pilih Akun</option>
                                
                                @foreach($akuns as $data)
                                    @if($data->tipe_saldo == 2 && $data->kategori_akun == 2 && $data->tipe_akun != 1)
                                        <option  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Akun Pajak Debit</label>
                            <select name="akun_pajak_debit_id" class="form-control">
                                <option>Pilih Akun</option>
                                @foreach($akuns as $data)
                                    @if($data->tipe_akun == 1 && $data->kategori_akun == 2)
                                        <option  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                    @endif
                                @endforeach
                            </select>
                            
                        </div>

                        <div class="form-group">
                            <label class="form-label">Akun Pajak Kredit</label>
                            <select name="akun_pajak_kredit_id" class="form-control">
                                <option>Pilih Akun</option>
                                
                                @foreach($akuns as $data)
                                    @if($data->tipe_saldo == 2 && $data->kategori_akun == 2 && $data->tipe_akun != 1)
                                        <option  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        
                       

                       
                        
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection