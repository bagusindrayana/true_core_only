@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Pajak</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
        <li class="breadcrumb-item active">Data Pajak</li>
        <li style="float: right;"><a href="{{ url($page.'/create') }}" class="btn btn-default">Tambah Data</a> </li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"></div>
                <div class="panel-body table-responsive">
                    
                    <table class="table display " id="example0">
                        <thead>
                            <tr>
                                <th>Jenis Transaksi</th>
                                <th>Akun Debit</th>
                                <th>Akun Kredit</th>
                                <th>Akun Pajak Debit</th>
                                <th>Akun Pajak Kredit</th>
                                
                               
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tables as $data)
                                <tr>
                                    <td>{{ $data->transaksi($data->jenis_transaksi) }}</td>
                                    <td>{{ $data->akun_debit->nama_akun }}</td>
                                    <td>{{ $data->akun_kredit->nama_akun }}</td>
                                    <td>{{ $data->akun_pajak_debit->nama_akun }}</td>
                                    <td>{{ $data->akun_pajak_kredit->nama_akun }}</td>
                                    
                                    <td class="aksi">
                                        <a href="{{ url($page.'/'. $data->id.'/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete<?php echo $data->id;  ?>').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url($page.'/'.$data->id) }}" id="delete<?php echo $data->id;  ?>" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection