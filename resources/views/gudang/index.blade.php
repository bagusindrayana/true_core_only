@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Gudang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
        <li class="breadcrumb-item active">Data Gudang</li>
       
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"></div>
                <div class="panel-body table-responsive pre-scrollable">
                    
                    <table class="table display">
                        <thead>
                            <tr>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Merek barang</th>
                                <th>Gambar Barang</th>
                                <th>Stok Barang</th>
                                <th>Harga Barang</th>
                              
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($gudangs as $data)
                                <tr>
                                    <td>{{ $data->barang->kode_barang }}</td>
                                    <td>{{ $data->barang->nama_barang }}</td>
                                    <td>{{ $data->barang->merek_barang }}</td>
                                    <td><img src="{{ asset('images/barang/'.$data->barang->gambar_barang)  }}" style="max-width: 40%;"></td>
                                    <td>{{ $data->jumlah }}</td>
                                    <td>Rp.{{ number_format($data->harga) }}</td>
                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection