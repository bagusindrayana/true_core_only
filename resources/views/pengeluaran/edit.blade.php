@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Edit Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Edit Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$pengeluaran->id) }}" method="POST">
                        {{ csrf_field() }}
                       
                        <input type="hidden" name="_method" value="PUT">
                        
                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" id="date-picker" name="tanggal" class="form-control" value="{{ $pengeluaran->tanggal }}" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <div class="form-group">
                            <label for="nomor" class="form-label" >Nomor</label>
                            <input type="text" name="nomor" class="form-control" value="{{ $pengeluaran->nomor }}">
                
                        </div>

                        <div class="form-group">
                            <label for="untuk" class="form-label" >Untuk</label>
                            <input type="text" name="untuk" class="form-control" value="{{ $pengeluaran->untuk }}">
                
                        </div> 
                  
                        <div class="form-group">
                            <label for="akun1" class="form-label" >Dari akun (Debit)</label>
                            
                            <select name="id_akun1" class="form-control">
                                <option>Pilih Akun</option>
                                @foreach($akuns as $data)
                                    @if($data->kategori_akun == 2 && $data->tipe_akun == 1)
                                        <option @if($data->id == $pengeluaran->id_akun_1) selected @endif  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                    @endif
                                @endforeach
                            </select>
                             
                
                        </div>

                        <div class="form-group">
                            <label for="akun2" class="form-label" >Ke akun (Kredit)</label>
                            <select name="id_akun2" class="form-control">
                                <option>Pilih Akun</option>
                                @foreach($akuns as $data)
                                    @if($data->tipe_saldo == 2 && $data->kategori_akun == 2 && $data->tipe_akun != 1)
                                        <option @if($data->id == $pengeluaran->id_akun_2) selected @endif  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                    @endif
                                @endforeach
                                
                            </select>
                
                        </div>

                      
                        <div style="clear: both;"></div>
                        <button type="submit" class="btn btn-default" name="tombol" value="ubah">Simpan</button>
                        
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Detail {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$pengeluaran->id) }}" method="POST">
                        {{ csrf_field() }}
                       
                        <input type="hidden" name="_method" value="PUT">
                        
                        <div class="form-group">
                            <label for="kode_unit" class="form-label" >Unit</label>
                            <select name="kode_unit" class="form-control">
                                <option>Pilih Unit</option>
                                @foreach($units as $data)
                                    
                                        <option  value="{{ $data->kode_unit }}">{{ $data->nama_unit }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>

                        <div class="form-group">
                            <label for="uraian" class="form-label">Uraian</label>
                            <input required type="text" class="form-control" id="uraian" name="uraian">
                        </div>

                        <div class="form-group">
                            <label for="qty" class="form-label" >QTY</label>
                            <input type="number" name="qty" required="" class="form-control">
                            
                
                        </div>

                        <div class="form-group">
                            <label for="harga_satuan" class="form-label" >Harga Satuan (Rp.)</label>
                            <input type="number" name="harga_satuan" required="" class="form-control">
                            
                
                        </div>
                        <div style="clear: both;"></div>
                         <button type="submit" class="btn btn-default" name="tombol" value="tambah_detail">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">Data Detail {{ $title }}  </div>
                <div class="panel-body table-responsive">
                    
                    <table class="table display">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Unit</th>
                               
                                <th>Urain</th>
                                <th>QTY</th>
                                <th>Harga Satuan</th>
                              
                                <th>Jumlah</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0;$no = 1; ?>
                            @foreach($pengeluaran->detail as $data)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $data->unit->nama_unit }}</td>
                                    <td>{{ $data->uraian }}</td>
                         
                                    <td>{{ $data->qty }}</td>
                                    <td>Rp.{{ number_format($data->harga_satuan,2) }}</td>
                                    <td>Rp.{{ number_format($data->jumlah,2) }}</td>
                                    
                                    
                                    <td>
                                        <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete-<?php echo $data->id; ?>').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url('detail_pengeluaran/'.$data->id) }}" id="delete-<?php echo $data->id; ?>" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                </tr>
                                <?php $total += $data->jumlah; ?>
                            @endforeach
                            <tr>
                                <td colspan="4"></td>
                                <td><strong>Total : </strong></td>
                                <td><strong>Rp.{{ number_format($total,2) }}</strong></td>
                                <td></td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection