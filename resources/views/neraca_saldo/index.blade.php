@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
        <li class="breadcrumb-item active">Data {{ $title }}</li>
     
    </ol>

    
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">
                    <form action="{{ url($page) }}" method="POST" target="_blank">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label  >Bulan :</label>
                            <select name="bulan" class=" col-md-2">
                                <option value="1">Januari</option>
                                <option value="2">Febuari</option>
                                <option value="3">Maret</option>
                                <option value="4">April</option>
                                <option value="5">Mei</option>
                                <option value="6">Juni</option>
                                <option value="7">Juli</option>
                                <option value="8">Agustus</option>
                                <option value="9">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                            <button class="btn btn-info">Buat Laporan</button>
                        </div>

                        
                    </form>
                      </div>
                      
                   
                    
                <div class="panel-body ">
                    <?php $total_aktiva = 0;$total_pasiva = 0; ?>
                    <div class="row" style="padding: 10px;">
                        <h2 style="margin-left: 10px;">Aktiva</h2>
                        <br>
                        <table class="table display col-md-12" style="float: left;">
                            <thead>
                                <tr>
                                    <th>Nomor Akun</th>
                                    <th>Nama Akun</th>     
                                    <th>Saldo</th>
                                    
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($akuns as $data)
                                    @if($data->jenis_akun == 1)
                                        <tr>                   
                                            <td>{{ $data->nomor_akun }}</td>
                                            <td >{{ $data->nama_akun }}</td>
                                            <td>Rp. {{ number_format($data->saldo) }}</td>
                                            <?php $total_aktiva += $data->saldo; ?>
                                          
                                        </tr>
                                    @endif
                                    
                                @endforeach
                                
                            </tbody>
                            <tfoot style="font-size: 20px;">
                                <tr>
                                    <td colspan="2">Total : </td>
                                    <td>Rp. {{ number_format($total_aktiva) }}</td>
                                </tr>
                            </tfoot>
                        </table>

                        
                    </div>
                    <div class="row" style="padding: 10px;">
                        <h2 style="margin-left: 10px;">Pasiva</h2>
                        <br>
                        <table class="table display col-md-12" style="float: right;">
                            <thead>
                                <tr>
                                    <th>Nomor Akun</th>
                                    <th>Nama Akun</th>     
                                    <th>Saldo</th>
                                    
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($akuns as $data)
                                    @if($data->jenis_akun == 2)
                                        <tr>                   
                                            <td>{{ $data->nomor_akun }}</td>
                                            <td >{{ $data->nama_akun }}</td>
                                            <td>Rp. {{ number_format($data->saldo) }}</td>
                                            <?php $total_pasiva += $data->saldo; ?>
                                          
                                        </tr>
                                    @endif
                                    
                                @endforeach

                                <?php 
                                    $pendapatan = 0;
                                    $biaya = 0;
                                 ?>
                                @foreach($akuns as $data)
                                    @if($data->sub_akun == 0 && $data->jenis_akun == 4)
                                        @foreach($akuns as $sub_data)
                                            @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                               
                                                    <?php $pendapatan += (int)$sub_data->saldo; ?>
                                                   
                                                @foreach($akuns as $sub_sub_data)
                                                    @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                                        
                                                            <?php $pendapatan += (int)$sub_sub_data->saldo; ?>
                                                            
                                                       
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @elseif($data->sub_akun == 0 && $data->jenis_akun == 5)
                                        @foreach($akuns as $sub_data)
                                            @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                                
                                                    <?php $biaya += (int)$sub_data->saldo; ?>
                                                    
                                                @foreach($akuns as $sub_sub_data)
                                                    @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                                        
                                                            <?php $biaya += (int)$sub_sub_data->saldo; ?>
                                                           
                                                       
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach

                                <tr>
                                    <td colspan="2">LABA /RUGI BULAN BERJALAN</td>
                                    <td>Rp. {{ number_format($pendapatan+$biaya) }}</td>
                                </tr>
                            </tbody>
                            
                            <tfoot style="font-size: 20px;">

                                <tr>
                                    <td colspan="2">Total : </td>
                                    <td>Rp. {{ number_format($total_pasiva+$pendapatan-$biaya) }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                        
                   
                </div>
                  
                        
                
            </div>
        </div>
    </div>
</div>


@endsection