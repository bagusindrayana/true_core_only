 <?php 
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition:attachment;filename=laporan_neraca_".date('F')."-".date("Y_m_d_h_i_s").".xls"); 
 ?>
<table >
     <tr  style="text-align: center;font-weight: bold;font-size: 20px;">
         <td colspan="5" >PT. TRAS RENTAL UTAMA EKAMULYA</td>
     </tr>
     <tr style="text-align: center;font-weight: bold;font-size: 20px;">
         <td colspan="5">Neraca</td>
     </tr style="text-align: center;font-weight: bold;font-size: 20px;">

     <tr style="text-align: center;font-weight: bold;font-size: 20px;">
         <td colspan="5">Periode {{ $bulan }}</td>
     </tr>
     <tr>
         <td colspan="5"><br><br><br><br></td>
     </tr>
 </table>
       
<?php $total_aktiva = 0;$total_pasiva = 0; ?>
                    <div class="row" style="padding: 10px;">
                        <h2 style="margin-left: 10px;">Aktiva</h2>
                        <br>
                        <table class="table display col-md-12" style="float: left;" border="1">
                            <thead>
                                <tr>
                                    <th>Nomor Akun</th>
                                    <th>Nama Akun</th>     
                                    <th>Saldo</th>
                                    
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($akuns as $data)
                                    @if($data->jenis_akun == 1)
                                        <tr>                   
                                            <td>{{ $data->nomor_akun }}</td>
                                            <td >{{ $data->nama_akun }}</td>
                                            <td>Rp. {{ number_format($data->saldo) }}</td>
                                            <?php $total_aktiva += $data->saldo; ?>
                                          
                                        </tr>
                                    @endif
                                    
                                @endforeach
                                
                            </tbody>
                            <tfoot style="font-size: 20px;">
                                <tr>
                                    <td colspan="2">Total : </td>
                                    <td>Rp. {{ number_format($total_aktiva) }}</td>
                                </tr>
                            </tfoot>
                        </table>

                        
                    </div>
                    <div class="row" style="padding: 10px;">
                        <h2 style="margin-left: 10px;">Pasiva</h2>
                        <br>
                        <table class="table display col-md-12" style="float: right;" border="1">
                            <thead>
                                <tr>
                                    <th>Nomor Akun</th>
                                    <th>Nama Akun</th>     
                                    <th>Saldo</th>
                                    
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($akuns as $data)
                                    
                                    @if($data->jenis_akun == 2)
                                        <tr>                   
                                            <td>{{ $data->nomor_akun }}</td>
                                            <td >{{ $data->nama_akun }}</td>
                                            <td>Rp. {{ number_format($data->saldo) }}</td>
                                            <?php $total_pasiva += $data->saldo; ?>
                                          
                                        </tr>
                                    @endif
                                    
                                @endforeach

                                <?php 
                                    $pendapatan = 0;
                                    $biaya = 0;
                                 ?>
                                @foreach($akuns as $data)
                                    @if($data->sub_akun == 0 && $data->jenis_akun == 4)
                                        @foreach($akuns as $sub_data)
                                            @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                               
                                                    <?php $pendapatan += (int)$sub_data->saldo; ?>
                                                   
                                                @foreach($akuns as $sub_sub_data)
                                                    @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                                        
                                                            <?php $pendapatan += (int)$sub_sub_data->saldo; ?>
                                                            
                                                       
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @elseif($data->sub_akun == 0 && $data->jenis_akun == 5)
                                        @foreach($akuns as $sub_data)
                                            @if($sub_data->sub_akun == $data->id && $sub_data->sub_akun != 0)
                                                
                                                    <?php $biaya += (int)$sub_data->saldo; ?>
                                                    
                                                @foreach($akuns as $sub_sub_data)
                                                    @if($sub_sub_data->sub_akun == $sub_data->id && $sub_sub_data->sub_akun != 0)
                                                        
                                                            <?php $biaya += (int)$sub_sub_data->saldo; ?>
                                                           
                                                       
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach

                                <tr>
                                    <td colspan="2">LABA /RUGI BULAN BERJALAN</td>
                                    <td>Rp. {{ number_format($pendapatan+$biaya) }}</td>
                                </tr>
                            </tbody>
                            
                            <tfoot style="font-size: 20px;">

                                <tr>
                                    <td colspan="2">Total : </td>
                                    <td>Rp. {{ number_format($total_pasiva+$pendapatan-$biaya) }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>