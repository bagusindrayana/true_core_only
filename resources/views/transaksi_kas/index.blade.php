@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Jurnal</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
        <li class="breadcrumb-item active">Data Jurnal</li>
     
    </ol>

    
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">
                    <form action="{{ url($page) }}" method="POST" target="_blank">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label for="nomor_akun" class=" col-md-2" >Dari Tanggal :</label>
                            <input type="date" class=" col-md-2" name="dari_tanggal">
                            <label for="nomor_akun" class=" col-md-2" >Sampai Tanggal :</label>
                            <input type="date" class=" col-md-2" name="sampai_tanggal"> 
                            <button class="btn btn-info">Buat Laporan</button>
                        </div>

                        
                    </form>
                      </div>
                <div class="panel-body table-responsive">
                    <?php 
                   
                    $debit = 0;
                    $kredit = 0;
                    ?>
                    <table  class="table display">
                        <thead>
                           
                            <tr>
                                <th>No</th>
                                <th >Tanggal</th>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                                <th>Nama Akun</th>
                                <th>Unit</th>
                                <th>Debit</th>
                                <th>Kredit</th>
                              
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($tagihan as $dt)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    <td>{{ $dt->no }}</td>
                                    <td>Piutang Penjualan dengan No.{{ $dt->no }}</td>
                                    <td>PIUTANG USAHA</td>
                                    <td>-</td>
                                    @if($dt->ppn == 1)
                                    <?php $ppn = $dt->saldo*(10/100); ?>
                                       
                                    @endif
                                    @if($dt->pph == 1)
                                    <?php $pph = $dt->saldo*(2/100); ?>
                                        
                                    @endif
                                    <td>Rp.{{ number_format($dt->saldo+$ppn-$pph) }}</td>
                                    <td>Rp.0</td>
                                    <?php $debit += $dt->saldo+$ppn-$pph; ?>
                                </tr>
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    <td>{{ $dt->no }}</td>
                                    <td>PPh 2% Invoice No.{{ $dt->no }}</td>
                                    <td>PAJAK DIBAYAR DIMUKA</td>
                                    <td>-</td>
                                    <td>Rp.{{ number_format($dt->saldo*(2/100)) }}</td>
                                    <td>Rp.0</td>
                                    <?php $debit += $dt->saldo*(2/100); ?>
                                </tr>
                                
                                @foreach($dt->detail as $data)

                                   <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $dt->tanggal }}</td>
                                        <td>{{ $dt->no }}</td>
                                        <td>{{ $data->uraian }}</td>
                                        <td>PENDAPATAN USAHA</td>
                                        <td>{{ $data->unit->nama_unit }}</td>
                                        <td>Rp.0</td>
                                        <td>Rp.{{ number_format($data->jumlah) }}</td>
                                        
                                        <?php $kredit += $data->jumlah; ?>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    <td>{{ $dt->no }}</td>
                                    <td>PPN No.{{ $dt->no }}</td>
                                    
                                    <td>UTANG PAJAK</td>
                                    <td>-</td>
                                    <td>Rp.0</td>
                                    <td>Rp.{{ number_format($dt->saldo*(10/100)) }}</td>
                                    <?php $kredit += $dt->saldo*(10/100); ?>
                                </tr>
                                
                            @endforeach
                           

                            @foreach($pemasukan as $dt)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    <td>{{ $dt->nomor }}</td>
                                    <td>{{ $dt->uraian}}</td>
                                    <td>{{ $dt->akun1->nama_akun}}</td>
                                    <td>-</td>
                                    <td>Rp.{{ number_format($dt->saldo) }}</td>
                                    <td>Rp.0</td>
                                    <?php $debit += $dt->saldo; ?>
                                    
                                </tr>

                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    <td>{{ $dt->nomor }}</td>
                                    <td>{{ $dt->uraian}}</td>
                                    <td>{{ $dt->akun2->nama_akun}}</td>
                                    <td>-</td>
                                    <td>Rp.0</td>
                                    <td>Rp.{{ number_format($dt->saldo) }}</td>
                                    <?php $kredit += $dt->saldo; ?>
                                </tr>

                             
                            @endforeach

                            @foreach($jurnalumum as $dt)
                                @foreach($dt->detail as $data)
                                   <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $dt->tanggal }}</td>
                                        <td>{{ $dt->no_jurnal }}</td>
                                        <td>{{ $dt->uraian }}</td>
                                        <td>{{$data->akun->nama_akun}}</td>
                                        <td>-</td>
                                        <td>Rp.{{ number_format($data->debit) }}</td>
                                        <td>Rp.{{ number_format($data->kredit) }}</td>
                                        <?php $kredit += $data->kredit; ?>
                                        <?php $debit += $data->debit ?>
                                        
                                    </tr>
                                @endforeach

                             
                            @endforeach

                            @foreach($pengeluaran as $dt)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    <td>{{ $dt->nomor }}</td>
                                    <td>{{ $dt->untuk}}</td>
                                    <td>{{ $dt->akun1->nama_akun}}</td>
                                    <td>-</td>
                                    <td>Rp.{{ number_format($dt->saldo) }}</td>
                                    <td>Rp.0</td>
                                    <?php $debit += $dt->saldo; ?>
                                    
                                </tr>

                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    <td>{{ $dt->nomor }}</td>
                                    <td>{{ $dt->untuk}}</td>
                                    <td>{{ $dt->akun2->nama_akun}}</td>
                                    <td>-</td>
                                    <td>Rp.0</td>
                                    <td>Rp.{{ number_format($dt->saldo) }}</td>
                                    <?php $kredit += $dt->saldo; ?>
                                    
                                </tr>

                             
                            @endforeach

                           
                            
                        </tbody>
                        <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Rp.{{ number_format($debit) }}</th>
                            <th>Rp.{{ number_format($kredit) }}</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script> 
<script type="text/javascript">
$(document).ready(function() {
      $('th').each(function(col) {
        
        if($(this).html() != "Tanggal"){
            return;
        }
          if ($(this).is('.asc')) {
            $(this).removeClass('asc');
            $(this).addClass('desc selected');
            sortOrder = -1;
          }
          else {
            $(this).addClass('asc selected');
            $(this).removeClass('desc');
            sortOrder = 1;
          }
          $(this).siblings().removeClass('asc selected');
          $(this).siblings().removeClass('desc selected');
          var arrData = $('table').find('tbody >tr:has(td)').get();
          arrData.sort(function(a, b) {
            var val1 = $(a).children('td').eq(col).text().toUpperCase();
            var val2 = $(b).children('td').eq(col).text().toUpperCase();
            if($.isNumeric(val1) && $.isNumeric(val2))
            return sortOrder == 1 ? val1-val2 : val2-val1;
            else
               return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
          });
          $.each(arrData, function(index, row) {
            $('tbody').append(row);
          });
        
      });
});
</script>

@endsection

