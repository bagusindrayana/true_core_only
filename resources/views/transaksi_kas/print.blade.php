<?php 
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition:attachment;filename=laporan_transaksi_kas-".date("Y_m_d_h_i_s").".xls"); 
 ?>
<?php 
                    $akun = \App\Akun::where('nama_akun','KAS')->first(); 
                    $saldo = $akun->saldo_awal;
                    //$saldo = 0;
                    ?>
                    <table >
                        <thead>
                            <!-- <th>
                                <td colspan="5"></td>
                                <td>Saldo Sebelumnya</td>
                                <td>{{ $akun->saldo_awal }}</td>
                            </th> -->
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Jenis Transaksi</th>
                                <th>Dari Akun</th>
                                <th>Ke Akun</th>
                                <th>Debit</th>
                                <th>Kredit</th>
                                <th>Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            <!-- @foreach($tagihan as $data)
                                <tr>
                                    <td>{{ $data->tanggal }}</td>
                                    <td>Tagihan</td>
                                    
                                    
                                </tr>
                            @endforeach -->

                            @foreach($pengeluaran as $data)
                                @if($data->akun2->id == $akun->id)
                                    <?php $saldo -= $data->saldo; ?>

                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data->tanggal }}</td>
                                        <td>Pengeluaran</td>
                                        <td>{{ $data->akun1->nama_akun }}</td>
                                        <td><!-- {{ $data->akun2->nama_akun }} --></td>
                                        <td></td>
                                        <td>{{ $data->saldo }}</td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach


                            @foreach($pemasukan as $data)
                                @if($data->akun1->id == $akun->id)
                                    <?php $saldo += $data->saldo; ?>
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data->tanggal }}</td>
                                        <td>Pemasukan</td>
                                        <td><!-- {{ $data->akun1->nama_akun }} --></td>
                                        <td>{{ $data->akun2->nama_akun }}</td>
                                        <td>{{ $data->saldo }}</td>
                                        <td></td>
                                        <td>{{ $saldo }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            
                        </tfoot>
                    </table>