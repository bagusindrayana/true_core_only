@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Detail Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Detail Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST" id="form_jurnal">
                        {{ csrf_field() }}
                        

                        

                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            
                                            <input disabled  readonly type="text" id="date-picker" name="tanggal" class="form-control" value="<?php echo $jurnalumum->tanggal ?>"/>
                                        
                        </div>

                        <div class="form-group">
                            <label for="no_jurnal" class="form-label" >No Jurnal</label>
                            <input disabled  readonly type="text" name="no_jurnal" class="form-control" required="" value="{{ $jurnalumum->no_jurnal }}">
                
                        </div> 
                        

                        <div class="form-group">
                            <label for="uraian" class="form-label">Uraian</label>
                            <input disabled  readonly required type="text" class="form-control" id="uraian" name="uraian" value="{{ $jurnalumum->uraian }}">
                        </div>
                        <div style="clear: both;"></div>

                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-title"> Detail Data Detail {{ $title }}
                    
                </div>
                <div class="panel-body">
                    
                    <table  class="table display">
                        <thead>
                            <tr>
                                <th>Akun</th>
                                <th>Debit</th>
                                <th>Kredit</th>
                                <th>Uraian</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="detail_jurnal">
                            <?php $total_debit = 0;$total_kredit = 0; ?>
                            @foreach($jurnalumum->detail as $data)

                                <tr  id="list-{{ $data->id }}">
                                    <td>
                                        <input disabled="" type="text" name="akun" value="{{ $data->akun->nama_akun }}">
                                    </td>
                                    <td>
                                        <input disabled  readonly type="number" name="debit[]"  value="{{ $data->debit }}" min="0"  class="input_debit">
                                    </td>
                                    <td>
                                        <input disabled  readonly type="number" name="kredit[]" value="{{ $data->kredit }}" min="0" class="input_kredit">
                                    </td>
                                    <td>
                                        <input disabled  readonly type="text" name="uraian_detail[]" value="{{ $data->uraian }}">
                                    </td>
                                    <td>
                                        
                                    </td>
                                </tr>
                                <?php $total_debit += $data->debit;$total_kredit += $data->kredit; ?>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td><input disabled  readonly type="number" id="total_debit" name="total_debit" value="{{ $total_debit }}" min="0"  readonly=""></td>
                                <td><input disabled  readonly type="number" id="total_kredit" name="total_kredit" value="{{ $total_kredit }}" min="0"  readonly=""></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        
                    </table>
                    
                    </form>
                    
                </div>
            </div>
        </div>
    </div>

</div>


@endsection