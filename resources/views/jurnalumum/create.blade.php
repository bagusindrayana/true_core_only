@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>
<form action="{{ url($page) }}" method="POST" id="form_jurnal">
<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    
                        {{ csrf_field() }}
                        

                        

                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" id="date-picker" name="tanggal" class="form-control" value="<?php echo date('m/d/Y') ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <div class="form-group">
                            <label for="no_jurnal" class="form-label" >No Jurnal</label>
                            <input type="text" name="no_jurnal" class="form-control" required="">
                
                        </div> 
                        

                        <div class="form-group">
                            <label for="uraian" class="form-label">Uraian</label>
                            <input required type="text" class="form-control" id="uraian" name="uraian">
                        </div>
                        <div style="clear: both;"></div>

                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Detail {{ $title }}
                    
                </div>
                <div class="panel-body">
                    
                    <table  class="table display">
                        <thead>
                            <tr>
                                <th>Akun</th>
                                <th>Debit</th>
                                <th>Kredit</th>
                                <th>Uraian</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="detail_jurnal">
                            

                            <tr  id="list-1">
                                <td>
                                    <select name="id_akun[]" class="form-control pilih-akun" required style="font-size: 13px;width: 200px;">
                                        <option value="0">Pilih Akun</option>
                                        @foreach($akuns as $data)
                                            <option  value="{{ $data->id }}">{{ $data->nomor_akun }} | {{ $data->nama_akun }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="number" name="debit[]" value="0" min="0"  class="input_debit">
                                </td>
                                <td>
                                    <input type="number" name="kredit[]" value="0" min="0" class="input_kredit">
                                </td>
                                <td>
                                    <textarea class="uraian" name="uraian_detail[]"></textarea>
                                </td>
                                <td>
                                    <span style="display: none;" class="fa fa-trash del_list" list-id="list-1"></span>
                                </td>
                            </tr>
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td><input type="number" id="total_debit" name="total_debit" value="0" min="0"  readonly=""></td>
                                <td><input type="number" id="total_kredit" name="total_kredit" value="0" min="0"  readonly=""></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        
                    </table>
                    <button type="button" id="add_item">Akun Baru</button>
                    <br>
                    <br>
                    <label>Out of Balance : </label> <input type="number" name="ofb" value="0" readonly min="0" id="ofb">
                    <br>
                    <br>
                    <button type="submit" class="btn btn-default">Tambah</button>
                    
                    
                </div>
            </div>
        </div>
    </div>

</div>
</form>

@endsection