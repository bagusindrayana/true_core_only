@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data Costomer</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
        <li class="breadcrumb-item active">Data Costomer</li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"><a href="{{ url($page.'/create') }}" class="btn btn-default">Tambah Data</a> <br><br> Data Costomer  </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Costomer</th>
                                <th>Alamat</th>
                                <th>No Telepon</th>
                                
                               
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($costomers as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->nama_costomer }}</td>
                                    <td>{{ $data->alamat }}</td>
                                    <td>{{ $data->no_telepon }}</td>
                                    
                                    
                                    <td class="aksi">
                                        <a href="{{ url($page.'/'. $data->id.'/edit') }}" class="fa fa-pencil"></a> <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('edit').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url($page.'/'.$data->id) }}" id="edit" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection