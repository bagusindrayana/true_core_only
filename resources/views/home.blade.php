@extends('layouts.template')

@section('content')


<!-- Start Container -->
<div class="container-default animated fadeInRight" style="min-height: 500px;"> <br>
               

        <!-- Start Row -->
        <div id="tour-12" class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-facebook rounded"> <span class="mini-stat-icon"><i class="fa fa-pencil-square-o fg-facebook"></i></span>
                    <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $tagihans }}">0</span> Tagihan </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-twitter rounded"> <span class="mini-stat-icon"><i class="fa fa-sign-in fg-twitter"></i></span>
                    <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $pemasukans }}">0</span> Pemasukan </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-googleplus rounded"> <span class="mini-stat-icon"><i class="fa fa-sign-out fg-googleplus"></i></span>
                    <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $pengeluarans }}">0</span> Pengeluaran </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-bitbucket rounded"> <span class="mini-stat-icon"><i class="fa fa-cubes fg-bitbucket"></i></span>
                    <div class="mini-stat-info"> <span class="counter" data-counter="counterup" data-value="{{ $barangs }}">0</span> Barang </div>
                </div>
            </div>
            
        </div>
        <!-- End Row --> 

                
    

</div>  


@endsection