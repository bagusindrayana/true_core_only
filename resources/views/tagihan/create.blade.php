@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Tambah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page) }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        

                        

                        <div class="form-group " >
                            <label for="tanggal" class="control-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" id="date-picker" name="tanggal" class="form-control" value="<?php echo date('m/d/Y') ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                      
                        <div class="form-group">
                            
                            <label for="no" class="form-label" >No</label>
                            <input type="text" name="no" required="" class=" form-control">
                        </div>

                        <div class="form-group">
                            <label for="site" class="form-label" >Site</label>
                            <input type="text" name="site" required="" class="form-control">
                        </div>

                        <div class="form-group" style="margin-left: 40%;">
                            <label for="ppn" class="control-label" >PPN</label>
                            <input type="checkbox" name="ppn"  value="1"> 
                            <label for="pph" class="control-label" >PPH</label>
                            <input type="checkbox" name="pph" value="1">
                        </div>
                  
                        <div class="form-group">
                            <label for="id_costomer" class="form-label" >Costomer</label>
                            <select name="id_costomer" class="form-control">
                                <option>Pilih Constomer</option>
                                @foreach($costomers as $data)
                                    
                                        <option  value="{{ $data->id }}">{{ $data->nama_costomer }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div> 
                        
                        

                        <div class="form-group">
                            <label for="untuk" class="form-label" ></label>
                            <h5>Detail Tagihan</h5>
                
                        </div>
                        
                        <div class="form-group">
                            <label for="kode_unit" class="form-label" >Unit</label>
                            <select name="kode_unit" class="form-control">
                                <option>Pilih Unit</option>
                                @foreach($units as $data)
                                    
                                        <option  value="{{ $data->kode_unit }}">{{ $data->nama_unit }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>

                        <div class="form-group">
                            <label for="uraian" class="form-label">Uraian</label>
                            <input type="text" required  class="form-control" id="uraian" name="uraian">
                        </div>

                        <div class="form-group">
                            <label for="qty" class="form-label" >QTY</label>
                            <input type="number" name="qty" required="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="harga_satuan" class="form-label" >Harga Satuan (Rp.)</label>
                            <input type="number" name="harga_satuan" required="" class="form-control">
                            
                
                        </div>
                       

                       
                        
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    

</div>


@endsection