@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Ubah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Ubah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form >
                        {{ csrf_field() }}
                        

                        <input disabled  type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input disabled type="text" id="date-picker" name="tanggal" class="form-control" value="<?php echo date('Y-m-d') ?>" value="{{ $tagihan->tanggal }}"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                      
                        <div class="form-group">
                            <label for="no" class="form-label" >No</label>
                            <input disabled type="text" name="no" required="" class="form-control" value="{{ $tagihan->no }}">
                        </div>

                        <div class="form-group">
                            <label for="site" class="form-label" >Site</label>
                            <input disabled type="text" name="site" required="" class="form-control" value="{{ $tagihan->site }}">
                        </div>

                        <div class="form-group" style="margin-left: 40%;">
                            <label for="ppn" class="control-label" >PPN</label>
                            <input type="checkbox" name="ppn" @if($tagihan->ppn == 1) checked @endif  value="1"> 
                            <label for="pph" class="control-label" >PPH</label>
                            <input type="checkbox"  name="pph" value="1" @if($tagihan->pph == 1) checked @endif>
                        </div>
                  
                        <div class="form-group">
                            <label for="id_costomer" class="form-label" >Costomer</label>
                            <input type="text" readonly value="{{ $tagihan->costomer->nama_costomer }}" class="form-control">
                
                        </div> 

                       
                       
                        <div style="clear: both;"></div>
                        
                    </form>
                </div>
            </div>
        </div>
        
    </div>
    

    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">Data Detail {{ $title }}  </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Unit</th>
                                <th>Urain</th>
                                <th>QTY</th>
                                <th>Harga Satuan</th>
                                <th>Jumlah</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0;$no = 1; ?>
                            @foreach($tagihan->detail as $data)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $data->unit->nama_unit }}</td>
                                    <td>{{ $data->uraian }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td>Rp.{{  number_format($data->harga_satuan,2) }}</td>
                                    <td>Rp.{{  number_format($data->jumlah,2) }}</td>
                                    
                                   
                                </tr>
                                <?php $total += $data->jumlah; ?>
                            @endforeach
                            <?php 
                                $total_semua = $total;
                                
                                
                     

                             ?>
                            <tr>
                                <td colspan="3"></td>
                                <td>DPP : </td>
                                <td>Rp.{{  number_format($total,2) }}</td>
                            </tr>
                            @if($tagihan->ppn == 1)
                            <?php $ppn = $total*(10/100);$total_semua ; ?>
                                <tr>
                                    <td colspan="3"></td>
                                    <td>PPN 10% : </td>
                                    <td>Rp.{{  number_format($ppn,2) }}</td>
                                </tr>
                            @endif
                            @if($tagihan->pph == 1)
                            <?php $pph = $total*(2/100);$total_semua ; ?>
                                <tr>
                                    <td colspan="3"></td>
                                    <td>PPh 2% : </td>
                                    <td>Rp.{{ number_format($pph,2) }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td colspan="4"></td>
                                <td><strong>Total : </strong></td>
                                <td><strong>Rp.{{ number_format($total_semua+$ppn-$pph,2) }}</strong></td>
                            </tr>
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection