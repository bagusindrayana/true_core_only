@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active">Data {{ $title }}</li>
        <li style="float: right;"><a href="{{ url($page.'/create') }}" class="btn btn-default">Tambah Data</a> </li>
    </ol>

    
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title"></div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>Invoice</th>
                                <th>Tanggal</th>
                                <th>NO</th>
                                <th>Site</th>
                                <th>Costomer</th>
                                
                                <th>User</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tagihans as $data)
                                <tr>
                                    <td>{{ $data->invoice }}</td>
                                    <td>{{ $data->tanggal }}</td>
                                    <td>{{ $data->no }}</td>
                                    <td>{{ $data->site }}</td>
                                    <td>{{ $data->costomer->nama_costomer }}</td>
                                  
                                    <td>{{ $data->user->name }}</td>
                                    <td class="aksi">
                                        <a href="{{ url($page.'/'. $data->invoice.'/edit') }}" class="fa fa-pencil"></a>
                                        <a href="{{ url($page.'/'. $data->invoice) }}" class="fa fa-info"></a>
                                        <a href="{{ url('print/'.$page.'/'.$data->invoice) }}" class="fa fa-print"></a>
                                        <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete<?php echo $data->invoice;  ?>').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url($page.'/'.$data->invoice) }}" id="delete<?php echo $data->invoice;  ?>" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                </tr>
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection