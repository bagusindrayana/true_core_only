@extends('layouts.template')

@section('content')

<div class="page-header">
    <h1 class="title">Data {{ $title }}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
        <li class="breadcrumb-item active"><a href="{{ url($page) }}">Data {{ $title }}</a></li>
        <li class="breadcrumb-item"><a href="#">Ubah Data</a></li>
    </ol>
</div>

<div class="container-padding animated fadeInRight"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Ubah Data Data {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$tagihan->invoice) }}" method="POST">
                        {{ csrf_field() }}
                        

                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" id="date-picker" name="tanggal" class="form-control" value="{{ date('m/d/Y',strtotime($tagihan->tanggal)) }}"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                      
                        <div class="form-group">
                            <label for="no" class="form-label" >No</label>
                            <input type="text" name="no" required="" class="form-control" value="{{ $tagihan->no }}">
                        </div>

                        <div class="form-group">
                            <label for="site" class="form-label" >Site</label>
                            <input type="text" name="site" required="" class="form-control" value="{{ $tagihan->site }}">
                        </div>

                     
                        <div class="form-group" style="margin-left: 40%;">
                            <label for="ppn" class="control-label" >PPN</label>
                            <input type="checkbox" name="ppn" @if($tagihan->ppn == 1) checked @endif  value="1"> 
                            <label for="pph" class="control-label" >PPH</label>
                            <input type="checkbox"  name="pph" value="1" @if($tagihan->pph == 1) checked @endif>
                        </div>
                  
                        <div class="form-group">
                            <label for="id_costomer" class="form-label" >Costomer</label>
                            <select name="id_costomer" class="form-control">
                                <option>Pilih Constomer</option>
                                @foreach($costomers as $data)
                                    
                                        <option @if($tagihan->id_costomer == $data->id) selected @endif  value="{{ $data->id }}">{{ $data->nama_costomer }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div> 

                        

                        <div style="clear: both;"></div>
                        <button type="submit" name="tombol" class="btn btn-default" value="ubah">Ubah</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-title"> Tambah Data Detail {{ $title }}
                    
                </div>
                <div class="panel-body">
                    <form action="{{ url($page.'/'.$tagihan->invoice) }}" method="POST">
                        {{ csrf_field() }}
                       
                        <input type="hidden" name="_method" value="PUT">
                        
                        <div class="form-group">
                            <label for="kode_unit" class="form-label" >Unit</label>
                            <select name="kode_unit" class="form-control">
                                <option>Pilih Unit</option>
                                @foreach($units as $data)
                                    
                                        <option  value="{{ $data->kode_unit }}">{{ $data->nama_unit }}</option>
                                    
                                @endforeach
                            </select>
                
                        </div>
                        
                        <div class="form-group">
                            <label for="uraian" class="form-label">Uraian</label>
                            <input required type="text" class="form-control" id="uraian" name="uraian">
                        </div>

                        <div class="form-group">
                            <label for="qty" class="form-label" >QTY</label>
                            <input type="number" name="qty" required="" class="form-control">
                            
                
                        </div>

                        <div class="form-group">
                            <label for="harga_satuan" class="form-label" >Harga Satuan (Rp.)</label>
                            <input type="number" name="harga_satuan" required="" class="form-control">
                            
                
                        </div>
                        <div style="clear: both;"></div>
                         <button type="submit" class="btn btn-default" name="tombol" value="tambah_detail">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    

    <div class="row"> 
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-title">Data Detail {{ $title }}  </div>
                <div class="panel-body table-responsive">
                    
                    <table id="example0" class="table display">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Unit</th>
                                <th>Urain</th>
                                <th>QTY</th>
                                <th>Harga Satuan</th>
                                <th>Jumlah</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0;$no = 1; ?>
                            @foreach($tagihan->detail as $data)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $data->unit->nama_unit }}</td>
                                    <td>{{ $data->uraian }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td>Rp.{{  number_format($data->harga_satuan,2) }}</td>
                                    <td>Rp.{{  number_format($data->jumlah,2) }}</td>
                                    
                                    
                                    <td>
                                        <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete-<?php echo $data->id; ?>').submit();" class="fa fa-trash"></a></td>
                                        <form action="{{ url('detail_tagihan/'.$data->id) }}" id="delete-<?php echo $data->id; ?>" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                </tr>
                                <?php $total += $data->jumlah; ?>
                            @endforeach
                            <?php 
                                $total_semua = $total;
                                
                                
                     

                             ?>
                            <tr>
                                <td colspan="4"></td>
                                <td>DPP : </td>
                                <td>Rp.{{  number_format($total,2) }}</td>
                            </tr>
                            @if($tagihan->ppn == 1)
                            <?php $ppn = $total*(10/100);$total_semua; ?>
                                <tr>
                                    <td colspan="4"></td>
                                    <td>PPN 10% : </td>
                                    <td>Rp.{{  number_format($ppn,2) }}</td>
                                </tr>
                            @endif
                            @if($tagihan->pph == 1)
                            <?php $pph = $total*(2/100);$total_semua ; ?>
                                <tr>
                                    <td colspan="4"></td>
                                    <td>PPh 2% : </td>
                                    <td>Rp.{{ number_format($pph,2) }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td colspan="4"></td>
                                <td><strong>Total : </strong></td>
                                <td><strong>Rp.{{ number_format($total_semua+$ppn-$pph,2) }}</strong></td>
                            </tr>
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection